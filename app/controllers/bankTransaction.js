const models = require('../models');
const { BankTransaction, BankAccount, Invoice, sequelize } = models;
const { errorResponse } = require('../helpers/errorResponse');
const { formatDateLocal, formatDate } = require('./../helpers/dateConverter');
const { journalEntryDescription } = require('./../repositories/journalEntryRepository');
const generateNumber = require('./../helpers/generateNumber');
const { PAYMENT_TYPE } = require('./../helpers/constants');

exports.createBankTransaction = async (payment, inv, t) => {
  const obj = {
    BankAccount_ID: payment.BankAccount_ID,
    KodeTransaksi: await generateNumber(BankTransaction, 'TRS', 'KodeTransaksi'),
    TransactionStatus: 'POSTED',
    isActive: 1,
    Business_ID: payment.Business_ID,
    DateTransaction: new Date(),
  };

  if ((payment.PaymentType = PAYMENT_TYPE.SEND)) {
    await BankTransaction.create(
      {
        ...obj,
        Description: `Pembayaran untuk invoice No. ${inv.InvoiceNo}`,
        Debit: 0,
        Credit: payment.GrandTotal,
      },
      { transaction: t }
    );
  } else if ((payment.PaymentType = PAYMENT_TYPE.RECEIVE)) {
    await BankTransaction.create(
      {
        ...obj,
        Description: `Pembayaran dari invoice No. ${inv.InvoiceNo}`,
        Debit: payment.GrandTotal,
        Credit: 0,
      },
      { transaction: t }
    );
  }
};

// exports.updateJournalEntry = async (req, inv, inv_lines, transaction) => {
//   for (const i of inv_lines) {
//     await BankTransaction.update(
//       {
//         ChartAccount_ID: i.ChartAccount_ID,
//         Name: i.Name,
//         Quantity: i.Quantity,
//         Price: i.Price,
//         PphTax: i.PphTax,
//         PpnTax: i.PpnTax,
//         MasterTax_ID: i.MasterTax_ID,
//         TotalAmount: i.TotalAmount,
//         Business_ID: i.Business_ID,
//         createdBy: req.user.username,
//         createdBy: req.user.username,
//       },
//       {
//         where: { id: i.id, Invoice_ID: inv.id },
//       },
//       { transaction }
//     );
//   }
// };

// exports.deleteJournalEntry = async (req, res, next) => {
//   try {
//     const { id } = req.params;

//     const invLine = await BankTransaction.findByPk(id);
//     if (!invLine) {
//       return res.status(404).send(errorResponse('Invoice Line not found'));
//     }

//     BankTransaction.destroy({ where: { id } });

//     return res.status(200).send({
//       message: 'Invoice Line was deleted',
//     });
//   } catch (error) {
//     next(error);
//   }
// };

exports.internalTransfer = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { toBankAccId, amount, reference } = req.body;

    // validate existance of issued bank account
    const baFrom = await BankAccount.findOne({
      where: { id: id },
    });
    if (!baFrom) {
      return res.status(404).send({
        message: 'Issued bank account not found',
      });
    }

    // validate existance of destination bank account
    const baTo = await BankAccount.findOne({
      where: { id: toBankAccId },
    });
    if (!baTo) {
      return res.status(404).send({
        message: 'Destination bank account not found',
      });
    }

    // update saldo issued bank account (saldo - amount)
    await BankAccount.update(
      {
        Saldo: baFrom.Saldo - amount,
      },
      {
        where: { id: id },
      }
    );

    // update saldo destination bank account (saldo + amount)
    await BankAccount.update(
      {
        Saldo: baTo.Saldo + amount,
      },
      {
        where: { id: toBankAccId },
      }
    );

    // create transaction for issued bank account
    await BankTransaction.create({
      BankAccount_ID: id,
      KodeTransaksi: await generateNumber(BankTransaction, 'TRS', 'KodeTransaksi'),
      TransactionStatus: 'POSTED',
      isActive: 1,
      Business_ID: '3',
      DateTransaction: new Date(),
      Description: `Pindah buku ke bank account ${baTo.NamaAccount}`,
      Debit: 0,
      Credit: amount,
    });

    // create transaction for destination bank account
    await BankTransaction.create({
      BankAccount_ID: toBankAccId,
      KodeTransaksi: await generateNumber(BankTransaction, 'TRS', 'KodeTransaksi'),
      TransactionStatus: 'POSTED',
      isActive: 1,
      Business_ID: '3',
      DateTransaction: new Date(),
      Description: `Pindah buku dari bank account ${baFrom.NamaAccount}`,
      Debit: amount,
      Credit: 0,
    });

    return res.status(200).send({
      message: 'Internal transfer/pindah buku done',
    });
  } catch (error) {
    next(error);
  }
};
