const { QueryTypes } = require('sequelize');
const models = require('./../models');
const { JournalEntry, sequelize } = models;
const { INV_TYPE } = require('./../helpers/constants');

exports.journalEntryDescription = (invType, invNo) => {
  switch (invType) {
    case INV_TYPE.PURCHASE:
      return `Inv Pembelian / Purchase Invoice No. ${invNo}`;
    case INV_TYPE.SALES:
      return `Inv Penjualan / Purchase Invoice No. ${invNo}`;
    default:
      break;
  }
};
