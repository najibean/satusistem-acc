'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class Invoice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.InvoiceLine, {
        foreignKey: 'Invoice_ID',
        as: 'invoice_line'
      })

      this.hasMany(models.PaymentLine, {
        foreignKey: 'Invoice_ID',
        as: 'payment_line'
      })

      this.hasOne(models.JournalEntry, {
        foreignKey: 'Invoice_ID',
        as: 'journal_entry'
      })

      this.belongsTo(models.MasterCurrency, {
        foreignKey: 'MasterCurrency_ID',
        as: 'master_currency'
      })

      this.belongsTo(models.Order, {
        foreignKey: 'Order_ID',
        as: 'order'
      })

      this.belongsTo(models.Partner, {
        foreignKey: 'Partner_ID',
        as: 'partner'
      })
    }
  }
  Invoice.init(
    {
      InvoiceNo: DataTypes.STRING,
      InvoiceDate: DataTypes.DATE,
      DueDate: DataTypes.DATE,
      Order_ID: DataTypes.UUID,
      MasterCurrency_ID: DataTypes.UUID,
      Partner_ID: DataTypes.UUID,
      Subtotal: DataTypes.DOUBLE,
      Discount: DataTypes.DOUBLE,
      PphTax: DataTypes.DOUBLE,
      PpnTax: DataTypes.DOUBLE,
      GrandTotal: DataTypes.DOUBLE,
      InvoiceType: DataTypes.STRING,
      InvoiceStatus: DataTypes.STRING,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Invoice',
      tableName: 'MInvoice',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  Invoice.beforeCreate((el) => (el.id = uuidv4()));
  return Invoice;
};
