'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MCATemplate', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
      },
      NamaAkun: {
        type: Sequelize.STRING,
      },
      NomorAkun: {
        type: Sequelize.STRING,
      },
      AkunType: {
        type: Sequelize.STRING,
      },
      isAktiva: {
        type: Sequelize.INTEGER,
      },
      TotalTransactionValue: {
        type: Sequelize.DOUBLE,
      },
      HasSub: {
        type: Sequelize.INTEGER,
      },
      NomorAkunHeader: {
        type: Sequelize.STRING,
      },
      isActive: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MCATemplate');
  },
};
