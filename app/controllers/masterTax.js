const models = require('../models');
const { MasterTax, sequelize } = models;
const { errorResponse } = require('../helpers/errorResponse');

exports.createMasterTax = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    let { TaxLabel, PphValue, PpnValue, Business_ID } = req.body;

    const tax = await MasterTax.create(
      {
        TaxLabel,
        PphValue,
        PpnValue,
        Business_ID,
        createdBy: req.user.username,
      },
      { transaction: t }
    );
    await t.commit();

    const taxCreated = await MasterTax.findOne({
      where: { id: tax.id },
    });

    return res.status(200).send({
      message: 'Tax created',
      data: taxCreated,
    });
  } catch (error) {
    next(error);
  }
};

exports.getAllMasterTax = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const tax = await MasterTax.findAll({
      attributes: {
        exclude: ['updated_at', 'deleted_at'],
      },
      order: [['updated_at', 'DESC']],
    });

    const data = tax.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: tax.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};

exports.getMasterTaxById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const tax = await MasterTax.findOne({
      where: { id },
      attributes: {
        exclude: ['deleted_at'],
      },
    });

    if (!tax) {
      return res.status(404).send(errorResponse('Tax not found'));
    }

    return res.status(200).send({
      data: tax,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateMasterTax = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const { id } = req.params;
    let { TaxLabel, PphValue, PpnValue, Business_ID } = req.body;

    const tax = await MasterTax.findByPk(id);
    if (!tax) {
      return res.status(404).send(errorResponse('Tax not found'));
    }

    await MasterTax.update(
      {
        TaxLabel,
        PphValue,
        PpnValue,
        Business_ID,
        createdBy: req.user.username,
      },
      {
        where: { id },
      },
      { transaction: t }
    );
    await t.commit();

    return res.status(200).send({
      message: 'Tax updated',
      data: await MasterTax.findOne({
        where: { id },
        attributes: {
          exclude: ['updated_at', 'deleted_at'],
        },
      }),
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteMasterTax = async (req, res, next) => {
  try {
    const { id } = req.params;

    const coa = await MasterTax.findByPk(id);
    if (!coa) {
      return res.status(404).send(errorResponse('Tax not found'));
    }

    MasterTax.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Tax was deleted',
    });
  } catch (error) {
    next(error);
  }
};

exports.getByBusinessId = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const taxes = await MasterTax.findAll({
      where: {
        Business_ID: req.params.businessid,
      },
    });

    if (taxes.length == 0) {
      return res.status(400).send({
        message: `Tax not found on business id ${req.params.businessid}`,
      });
    }

    const data = taxes.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: taxes.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};
