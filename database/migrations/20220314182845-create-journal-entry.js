'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MJournalEntry', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      NomorJournal: {
        type: Sequelize.STRING,
      },
      TanggalJournal: {
        type: Sequelize.DATE,
      },
      Description: {
        type: Sequelize.STRING,
      },
      TotalCredit: {
        type: Sequelize.DOUBLE,
      },
      TotalDebit: {
        type: Sequelize.DOUBLE,
      },
      Status: {
        type: Sequelize.STRING,
      },
      MasterCurrency_ID: {
        type: Sequelize.UUID,
      },
      isGenerated: {
        type: Sequelize.INTEGER,
      },
      isActive: {
        type: Sequelize.INTEGER,
      },
      Business_ID: {
        type: Sequelize.STRING,
      },
      createdBy: {
        type: Sequelize.STRING,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MJournalEntry', {
      fields: ['MasterCurrency_ID'],
      type: 'foreign key',
      name: 'fk_journalentry_mastercurrency_id',
      references: {
        table: 'MMasterCurrency',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MJournalEntry');
  },
};
