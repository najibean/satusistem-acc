const { body } = require('express-validator');
const { ORDER_TYPE, ORDER_STATUS } = require('../helpers/constants');
const models = require('../models');
const { MasterTax, Partner, sequelize } = models;

const orderValidator = [
  body('OrderDate')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isISO8601()
    .toDate()
    .withMessage('Please use the correct date format (YYYY-MM-DD)'),
  body('DeliveryDate')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isISO8601()
    .toDate()
    .withMessage('Please use the correct date format (YYYY-MM-DD)'),
  body('OrderRef').isString().withMessage('Must be string'),
  body('Discount').isNumeric().withMessage('Must be contain number only'),
  body('isActive').isNumeric().withMessage('Must be contain number only'),
  body('Business_ID').isString().withMessage('Must be string'),
  body('OrderType')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom((value, { req }) => {
      let arr = [];
      for (const key in ORDER_TYPE) {
        arr.push(ORDER_TYPE[key]);
      }

      if (!Object.values(ORDER_TYPE).includes(value.toUpperCase())) {
        throw new Error('Please input: ' + arr.join(' or '));
      }

      return true;
    }),
  body('Partner_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const partner = await Partner.findByPk(value);

      if (!partner) {
        throw new Error('Please input the correct ID of Partner');
      }

      return true;
    }),
  body('order_line.*.MasterTax_ID')
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const tax = await MasterTax.findByPk(value);

      if (!tax) {
        throw new Error('Please input the correct ID of MasterTax');
      }

      return true;
    }),
  body('order_line.*.Name').isString().withMessage('Must be string'),
  body('order_line.*.Description').isString().withMessage('Must be string'),
  body('order_line.*.Quantity').isNumeric().withMessage('Must be contain number only'),
  body('order_line.*.Price').isNumeric().withMessage('Must be contain number only'),
  body('order_line.*.Business_ID').isString().withMessage('Must be string'),
];

module.exports = { orderValidator };
