const { QueryTypes } = require('sequelize');
const models = require('./../models');
const { Payment, Invoice, sequelize } = models;
const { INV_STATUS } = require('./../helpers/constants');
const { BadRequestException } = require('./../helpers/customException');

exports.paymentLineHandler = async (paymentLines) => {
  let arr = [];

  await Promise.all(
    paymentLines.map(async (el) => {
      const inv = await Invoice.findByPk(el.invoice_id);

      if (!!inv) {
        arr.push({
          // Invoice_ID: inv.id,
          Amount: el.amount_paid,
          invoice: inv,
        });
      }
    })
  );

  if (arr.length !== paymentLines.length) {
    throw new BadRequestException('One of invoice in payment_invoice array does not exist');
  }

  return arr;
};

exports.sumFromPaymentLine = async (invLine, key) => {
  if (invLine.length > 0) {
    return invLine.reduce((a, b) => {
      return a + b[key];
    }, 0);
  } else {
    return 0;
  }
};

// exports.invoiceValidatorHandler = async (paymentLines) => {
//   let invArr = [];

//   await Promise.all(
//     paymentLines.map(async (el) => {
//       const inv = await Invoice.findByPk(el.invoice_id);
//       if (inv) {
//         invArr.push(inv);
//       }
//     })
//   );

//   if (invArr.length < 1) {
//     return false;
//   }

//   return invArr;
// };

exports.paymentOnInvoice = async (inv, amount_paid, t) => {
  await Invoice.update(
    {
      GrandTotal: inv.GrandTotal - amount_paid,
      InvoiceStatus:
        amount_paid === 0
          ? INV_STATUS.UNPAID
          : inv.GrandTotal === amount_paid
          ? INV_STATUS.PAID
          : INV_STATUS.PAID_PARTIALLY,
    },
    {
      where: { id: inv.id },
    },
    {
      transaction: t,
    }
  );
};
