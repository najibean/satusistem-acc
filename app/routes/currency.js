const router = require('express').Router();
const { isAuthenticate } = require('../middlewares/authMiddleware');
const {
  createCurrency,
  getAllCurrency,
  getCurrencyById,
  updateCurrency,
  deleteCurrency,
} = require('../controllers/currency');
const { validate } = require('../validators');
const { paymentValidator } = require('../validators/paymentValidator');

/**
 * @swagger
 * components:
 *  schemas:
 *    Currency:
 *      type: object
 *      required: true
 *      properties:
 *        id:
 *          type: string
 *        CurrencyLabel:
 *          type: string
 *        Business_ID:
 *          type: string
 *        createdBy:
 *          type: string
 *        created_at:
 *          type: string
 *        updated_at:
 *          type: string
 *        deleted_at:
 *          type: string
 *  parameters:
 *    currency_id:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Currency ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/currency/all:
 *  get:
 *    summary: Returns the list of all currencies
 *    tags: [Currency]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of currencies
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Currency'
 */
router.get('/all', getAllCurrency);

/**
 * @swagger
 * /api/currency/create:
 *  post:
 *    summary: Create new currency
 *    tags: [Currency]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              CurrencyLabel:
 *                type: string
 *              Business_ID:
 *                type: string
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Payment was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Currency'
 *      500:
 *        description: Some server error
 */
router.post('/create', createCurrency);

/**
 * @swagger
 * /api/currency/{id}:
 *  get:
 *    summary: Get currency by id
 *    tags: [Currency]
 *    parameters:
 *      - $ref: '#/components/parameters/currency_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of currency
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Currency'
 *      404:
 *        description: Currency not found
 */
router.get('/:id', getCurrencyById);

/**
 * @swagger
 * /api/currency/{id}:
 *  patch:
 *    summary: Update currency by id
 *    tags: [Currency]
 *    parameters:
 *      - $ref: '#/components/parameters/currency_id'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Currency'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: currency was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Currency'
 *      404:
 *        description: currency not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', updateCurrency);

/**
 * @swagger
 * /api/currency/{id}:
 *  delete:
 *    summary: Remove Currency by id
 *    tags: [Currency]
 *    parameters:
 *      - $ref: '#/components/parameters/currency_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Currency was deleted
 *      404:
 *        description: Currency not found
 */
router.delete('/:id', deleteCurrency);

/*
 * @swagger
 * /api/currency/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of payments based on business id
 *    tags: [Currency]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of payments based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/business_id'
 */
// router.get('/all/by-business-id/:businessid', getByBusinessId);

module.exports = router;
