const models = require('../models');
const { Invoice, InvoiceLine, sequelize } = models;
const { Op } = require('sequelize');
const { errorResponse } = require('../helpers/errorResponse');
const { createInvoiceLine, updateInvoiceLine } = require('./invoiceLine');
const { formatDateLocal } = require('./../helpers/dateConverter');
const { createJournalEntry } = require('./journalEntry');
const { sumFromInvLine, invoiceLineHandler } = require('./../repositories/invoiceRepository');
const { INV_STATUS } = require('./../helpers/constants');
const generateNumber = require('./../helpers/generateNumber');
const pagination = require('./../helpers/pagination');

exports.createInvoice = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    let {
      InvoiceDate,
      DueDate,
      Order_ID,
      MasterCurrency_ID,
      Partner_ID,
      Discount,
      InvoiceType,
      isActive,
      Business_ID,
      invoice_line,
    } = req.body;

    const invoiceLineRepo = await invoiceLineHandler(invoice_line);

    const Subtotal = sumFromInvLine(invoiceLineRepo, 'TotalAmount');
    const PphTax = sumFromInvLine(invoiceLineRepo, 'PphTax');
    const PpnTax = sumFromInvLine(invoiceLineRepo, 'PpnTax');
    const GrandTotal = sumFromInvLine(invoiceLineRepo, 'GrandTotal');

    const inv = await Invoice.create(
      {
        InvoiceNo: await generateNumber(Invoice, 'INV', 'InvoiceNo'),
        InvoiceDate: formatDateLocal(InvoiceDate),
        DueDate: formatDateLocal(DueDate),
        Order_ID,
        MasterCurrency_ID,
        Partner_ID,
        Subtotal,
        Discount,
        PphTax,
        PpnTax,
        GrandTotal,
        InvoiceType: InvoiceType.toUpperCase(),
        InvoiceStatus: INV_STATUS.UNPAID,
        isActive,
        Business_ID,
        createdBy: req.user.username,
      },
      { transaction: t }
    );

    if (invoice_line.length > 0) {
      await createInvoiceLine(req, inv, invoiceLineRepo, t);
    }

    await createJournalEntry(req, inv, invoiceLineRepo, t);

    await t.commit();

    const invCreated = await Invoice.findOne({
      where: { id: inv.id },
      include: {
        model: InvoiceLine,
        as: 'invoice_line',
      },
    });

    return res.status(200).send({
      message: 'Invoice created',
      data: invCreated,
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.getAllInvoice = async (req, res, next) => {
  try {
    const { page, pagination } = req.query;

    const inv = await Invoice.findAll({
      attributes: {
        exclude: ['updated_at', 'deleted_at'],
      },
      include: {
        model: InvoiceLine,
        as: 'invoice_line',
      },
      order: [['updated_at', 'DESC']],
    });

    const data = inv.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: inv.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};

exports.getInvoiceById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const inv = await Invoice.findOne({
      where: { id },
      attributes: {
        exclude: ['deleted_at'],
      },
      include: {
        model: InvoiceLine,
        as: 'invoice_line',
      },
    });

    if (!inv) {
      return res.status(404).send(errorResponse('Invoice not found'));
    }

    return res.status(200).send({
      data: inv,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateInvoice = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const { id } = req.params;
    let {
      InvoiceDate,
      DueDate,
      Order_ID,
      MasterCurrency_ID,
      Partner_ID,
      Discount,
      InvoiceType,
      isActive,
      Business_ID,
      invoice_line,
    } = req.body;

    const inv = await Invoice.findByPk(id);
    if (!inv) {
      return res.status(404).send(errorResponse('Invoice not found'));
    }
    const invoiceLineRepo = await invoiceLineHandler(invoice_line);

    const Subtotal = sumFromInvLine(invoiceLineRepo, 'TotalAmount');
    const PphTax = sumFromInvLine(invoiceLineRepo, 'PphTax');
    const PpnTax = sumFromInvLine(invoiceLineRepo, 'PpnTax');
    const GrandTotal = sumFromInvLine(invoiceLineRepo, 'GrandTotal');

    await Invoice.update(
      {
        InvoiceNo: inv.InvoiceNo,
        InvoiceDate: formatDateLocal(InvoiceDate),
        DueDate: formatDateLocal(DueDate),
        Order_ID,
        MasterCurrency_ID,
        Partner_ID,
        Subtotal,
        Discount,
        PphTax,
        PpnTax,
        GrandTotal,
        InvoiceType: InvoiceType.toUpperCase(),
        InvoiceStatus: inv.InvoiceStatus,
        isActive,
        Business_ID,
        createdBy: req.user.username,
      },
      {
        where: { id },
      },
      { transaction: t }
    );

    if (invoice_line.length > 0) {
      await updateInvoiceLine(req, inv, invoice_line, t);
    }

    await t.commit();

    return res.status(200).send({
      message: 'Invoice updated',
      data: await Invoice.findOne({
        where: { id },
        attributes: {
          exclude: ['updated_at', 'deleted_at'],
        },
        include: {
          model: InvoiceLine,
          as: 'invoice_line',
        },
      }),
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.deleteInvoice = async (req, res, next) => {
  try {
    const { id } = req.params;

    const inv = await Invoice.findByPk(id);
    if (!inv) {
      return res.status(404).send(errorResponse('Invoice not found'));
    }

    Invoice.destroy({ where: { id } });
    InvoiceLine.destroy({ where: { Invoice_ID: id } });

    return res.status(200).send({
      message: 'Invoice was deleted',
    });
  } catch (error) {
    next(error);
  }
};

exports.unpaidInvoices = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const inv = await Invoice.findAll({
      where: { InvoiceStatus: { [Op.or]: [INV_STATUS.UNPAID, INV_STATUS.PAID_PARTIALLY] } },
      attributes: {
        exclude: ['updated_at', 'deleted_at'],
      },
      include: {
        model: InvoiceLine,
        as: 'invoice_line',
      },
      order: [['updated_at', 'DESC']],
    });

    const data = inv.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: inv.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};

exports.getByBusinessId = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const inv = await Invoice.findAll({
      where: {
        Business_ID: req.params.businessid,
      },
    });

    if (inv.length == 0) {
      return res.status(400).send({
        message: `Invoice not found on business id ${req.params.businessid}`,
      });
    }

    const data = inv.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: inv.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};

exports.getInvoiceByPartnerId = async (req, res, next) => {
  try {
    const { page, paginate } = req.query;

    const inv = await Invoice.findAll({
      where: {
        Partner_ID: req.params.partnerId,
      },
      ...pagination(page, paginate),
    });

    if (inv.length == 0) {
      return res.status(400).send({
        message: `Invoice not found on this partner id`,
      });
    }

    return res.status(200).send({
      total: inv.length,
      data: inv,
    });
  } catch (error) {
    next(error);
  }
};
