const { body } = require('express-validator');
const models = require('../models');
const { MasterCurrency, ChartAccount, ListBank, sequelize } = models;
const businessIdValidator = require('./businessIdValidator');

const bankAccountValidator = [
  body('ListBank_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .custom(async (value, { req }) => {
      const lb = await ListBank.findByPk(value);

      if (!lb) {
        throw new Error('Please input the correct ID from List Bank');
      }

      return true;
    }),
  body('NamaAccount')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('NoAccount')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .custom((value, { req }) => {
      if (isNaN(value)) {
        throw new Error('Must be number');
      }

      return true;
    }),
  body('BranchName')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('BranchCity')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('MasterCurrency_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .custom(async (value, { req }) => {
      const mc = await MasterCurrency.findByPk(value);

      if (!mc) {
        throw new Error('Please input the correct ID from Master Currency');
      }

      return true;
    }),
];

const createBankAccountValidator = [...bankAccountValidator, ...businessIdValidator];

module.exports = { bankAccountValidator, createBankAccountValidator };
