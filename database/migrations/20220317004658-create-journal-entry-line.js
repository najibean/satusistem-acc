'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MJournalEntryLine', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      JournalEntry_ID: {
        type: Sequelize.UUID
      },
      Partner_ID: {
        type: Sequelize.UUID
      },
      ChartAccount_ID: {
        type: Sequelize.UUID
      },
      debit: {
        type: Sequelize.DOUBLE
      },
      credit: {
        type: Sequelize.DOUBLE
      },
      isActive: {
        type: Sequelize.INTEGER
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MJournalEntryLine', {
      fields: ['JournalEntry_ID'],
      type: 'foreign key',
      name: 'fk_journalentryline_journalentry_id',
      references: {
        table: 'MJournalEntry',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MJournalEntryLine', {
      fields: ['Partner_ID'],
      type: 'foreign key',
      name: 'fk_journalentryline_partner_id',
      references: {
        table: 'MPartner',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MJournalEntryLine', {
      fields: ['ChartAccount_ID'],
      type: 'foreign key',
      name: 'fk_journalentryline_chartaccount_id',
      references: {
        table: 'MChartAccount',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MJournalEntryLine');
  }
};