'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class PaymentLine extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Invoice, {
        foreignKey: 'Invoice_ID',
        as: 'invoice'
      })

      this.belongsTo(models.Payment, {
        foreignKey: 'Payment_ID',
        as: 'payment'
      })
    }
  }
  PaymentLine.init(
    {
      Payment_ID: DataTypes.UUID,
      Invoice_ID: DataTypes.UUID,
      Amount: DataTypes.DOUBLE,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'PaymentLine',
      tableName: 'MPaymentLine',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  PaymentLine.beforeCreate((el) => (el.id = uuidv4()));
  return PaymentLine;
};
