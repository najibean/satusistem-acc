'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn('MJournalEntry', 'Invoice_ID', {
      type: Sequelize.UUID,
      after: 'MasterCurrency_ID',
    });

    await queryInterface.addConstraint('MJournalEntry', {
      fields: ['Invoice_ID'],
      type: 'foreign key',
      name: 'fk_journalentry_invoice_id',
      references: {
        table: 'MInvoice',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeConstraint('MJournalEntry', 'fk_journalentry_invoice_id');

    await queryInterface.removeColumn('MJournalEntry', 'Invoice_ID');
  },
};
