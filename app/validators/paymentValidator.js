const { body } = require('express-validator');
const { PAYMENT_METHOD, PAYMENT_TYPE, PAYMENT_STATUS } = require('../helpers/constants');
const models = require('../models');
const { Partner, BankAccount, Invoice, sequelize } = models;
const businessIdValidator = require('./businessIdValidator');

const paymentValidator = [
  body('Partner_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .custom(async (value, { req }) => {
      const partnerFound = await Partner.findByPk(value);

      if (!partnerFound) {
        throw new Error('Please input the correct ID from Partner');
      }

      return true;
    }),
  body('BankAccount_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .custom(async (value, { req }) => {
      const ba = await BankAccount.findByPk(value);

      if (!ba) {
        throw new Error('Please input the correct ID from Bank Account');
      }

      return true;
    }),
  body('PaymentType')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom((value, { req }) => {
      let arr = [];
      for (const key in PAYMENT_TYPE) {
        arr.push(PAYMENT_TYPE[key]);
      }

      if (!Object.values(PAYMENT_TYPE).includes(value.toUpperCase())) {
        throw new Error('Please input: ' + arr.join(' or '));
      }

      return true;
    }),
  body('PaymentMethod')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom((value, { req }) => {
      let arr = [];
      for (const key in PAYMENT_METHOD) {
        arr.push(PAYMENT_METHOD[key]);
      }

      if (!Object.values(PAYMENT_METHOD).includes(value.toUpperCase())) {
        throw new Error('Please input: ' + arr.join(' or '));
      }

      return true;
    }),
  body('DatePayment')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isISO8601()
    .toDate()
    .withMessage('Please use the correct date format (YYYY-MM-DD)'),
  body('Description')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('isActive')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .custom((value, { req }) => {
      if (typeof value !== 'number') {
        throw new Error('Must be number');
      }

      if (![0, 1].includes(value)) {
        throw new Error('Input must be 1 or 0');
      }

      return true;
    }),
  body('payment_invoice.*.invoice_id')
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const inv = await Invoice.findByPk(value);

      if (!inv) {
        throw new Error('Please input the correct ID of invoice_id');
      }

      return true;
    }),
];

const createPaymentValidator = [...paymentValidator, ...businessIdValidator];

module.exports = { paymentValidator, createPaymentValidator };
