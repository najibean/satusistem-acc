const { QueryTypes } = require('sequelize');
const models = require('./../models');
const { Partner, sequelize } = models;
const { PARTNER_TYPE } = require('./../helpers/constants');

exports.amountBasedOnPartnerId = async (id_partner) => {
  const queryResult = await sequelize.query(
    `
    SELECT 
      a.id, a.PartnerName, a.PartnerType, a.amount, 
      b.total_payment, (a.amount - b.total_payment) AS 'outstanding_amount'
    FROM 
      (
        SELECT 
          p.id, p.PartnerName, p.PartnerType, 
          SUM(i.GrandTotal) AS 'amount' 
        FROM MPartner p
        JOIN MInvoice i ON i.Partner_ID = p.id 
        WHERE p.id = '${id_partner}'
      ) AS a
    JOIN
      (
        SELECT 
          p.id, p.PartnerName, p.PartnerType, 
          SUM(p2.GrandTotal) AS 'total_payment' 
        FROM MPartner p
        JOIN MPayment p2 ON p2.Partner_ID = p.id 
        WHERE p.id = '${id_partner}'
      ) AS b ON b.id = a.id
    `,
    { type: QueryTypes.SELECT }
  );

  return queryResult[0];
};

const seqNumb = (partnerNo) => {
  return String(Number(partnerNo.slice(2)) + 1).padStart(3, 0);
};

exports.partnerNumber = async (type) => {
  try {
    if (type === PARTNER_TYPE.CLIENT) {
      const partners = await Partner.findAll({
        where: { PartnerType: PARTNER_TYPE.CLIENT },
      });
      if (partners.length > 0) {
        const lastPartnerNo = partners[partners.length - 1].PartnerNo;
        return 'CL' + seqNumb(lastPartnerNo);
      } else {
        return 'CL001';
      }
    } else if (type === PARTNER_TYPE.MULTI) {
      const partners = await Partner.findAll({
        where: { PartnerType: PARTNER_TYPE.MULTI },
      });
      if (partners.length > 0) {
        const lastPartnerNo = partners[partners.length - 1].PartnerNo;
        return 'MT' + seqNumb(lastPartnerNo);
      } else {
        return 'MT001';
      }
    } else if (type === PARTNER_TYPE.VENDOR) {
      const partners = await Partner.findAll({
        where: { PartnerType: PARTNER_TYPE.VENDOR },
      });
      if (partners.length > 0) {
        const lastPartnerNo = partners[partners.length - 1].PartnerNo;
        return 'VN' + seqNumb(lastPartnerNo);
      } else {
        return 'VN001';
      }
    }
  } catch (error) {
    console.log(error);
  }
};

