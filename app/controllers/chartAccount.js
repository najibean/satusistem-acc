const models = require('./../models');
const { ChartAccount, sequelize } = models;
const { ACCOUNT_TYPE } = require('./../helpers/constants');
const { errorResponse } = require('./../helpers/errorResponse');

exports.createChartAccount = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    let { AccountName, AccountNumber, HeaderID, AccountType, Business_ID } = req.body;

    let arrAccType = [];
    for (const key in ACCOUNT_TYPE) {
      arrAccType.push(ACCOUNT_TYPE[key]);
    }

    if (!Object.values(ACCOUNT_TYPE).includes(AccountType)) {
      return res
        .status(400)
        .json(errorResponse('Please fill AccountType: ' + arrAccType.join(' OR ')));
    }

    const coa = await ChartAccount.create(
      {
        AccountName,
        AccountNumber,
        HeaderID,
        AccountType,
        Business_ID,
        createdBy: req.user.username,
      },
      { transaction: t }
    );
    await t.commit();

    const coaCreated = await ChartAccount.findOne({
      where: { id: coa.id },
    });

    return res.status(200).send({
      message: 'Chart Account created',
      data: coaCreated,
    });
  } catch (error) {
    next(error);
  }
};

exports.getAllChartAccounts = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const coa = await ChartAccount.findAll({
      attributes: {
        exclude: ['updated_at', 'deleted_at'],
      },
      order: [['updated_at', 'DESC']],
    });

    const data = coa.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: coa.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};

exports.getChartAccountById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const coa = await ChartAccount.findOne({
      where: { id },
      attributes: {
        exclude: ['deleted_at'],
      },
    });

    if (!coa) {
      return res.status(404).send(errorResponse('Chart Account not found'));
    }

    return res.status(200).send({
      data: coa,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateChartAccount = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const { id } = req.params;
    let { AccountName, AccountNumber, HeaderID, AccountType, Business_ID } = req.body;

    let arrAccType = [];
    for (const key in ACCOUNT_TYPE) {
      arrAccType.push(ACCOUNT_TYPE[key]);
    }

    if (!Object.values(ACCOUNT_TYPE).includes(AccountType)) {
      return res
        .status(400)
        .json(errorResponse('Please fill AccountType: ' + arrAccType.join(' OR ')));
    }

    const coa = await ChartAccount.findByPk(id);
    if (!coa) {
      return res.status(404).send(errorResponse('Chart Account not found'));
    }

    await ChartAccount.update(
      {
        AccountName,
        AccountNumber,
        HeaderID,
        AccountType,
        Business_ID,
        createdBy: req.user.username,
      },
      {
        where: { id },
      },
      { transaction: t }
    );
    await t.commit();

    return res.status(200).send({
      message: 'Chart Account updated',
      data: await ChartAccount.findOne({
        where: { id },
        attributes: {
          exclude: ['updated_at', 'deleted_at'],
        },
      }),
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteChartAccount = async (req, res, next) => {
  try {
    const { id } = req.params;

    const coa = await ChartAccount.findByPk(id);
    if (!coa) {
      return res.status(404).send(errorResponse('Chart Account not found'));
    }

    ChartAccount.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Chart Account was deleted',
    });
  } catch (error) {
    next(error);
  }
};

exports.getByBusinessId = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const coa = await ChartAccount.findAll({
      where: {
        Business_ID: req.params.businessid,
      },
    });

    if (coa.length == 0) {
      return res.status(400).send({
        message: `Chart account not found on business id ${req.params.businessid}`,
      });
    }

    const data = coa.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: coa.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};
