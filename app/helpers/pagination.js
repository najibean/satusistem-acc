module.exports = (page = 1, paginate = 10) => {
  return {
    limit: Number(paginate),
    offset: (page - 1) * Number(paginate),
    order: [['updated_at', 'DESC']],
  };
};
