const express = require('express');
const cors = require('cors');
const compression = require('compression');
const helmet = require('helmet');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const configSwagger = require('./../config/swagger');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const router = require('./routes/index');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 3000;

const swaggerDocs = swaggerJsDoc(configSwagger);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use(cors({
  origin: [`http://localhost:${port}`],
  optionsSuccessStatus: 200
}));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(compression());
app.use(morgan('dev'));
app.use(cookieParser());
app.use(helmet());

if (process.env.NODE_ENV !== 'test') {
  app.listen(port, () => {
    console.log(`Running on port: ${port}`);
  });
}

app.use('/api', router);
app.use((req, res) => res.status(404).send('Page not found'));

// error handler
app.use((err, req, res, next) => {
  const isDev = req.app.get('env') === 'development';
  res.locals.error = err;
  console.log('Error:', err.message);
  const message = isDev ? err.message : 'Internal server error';
  res.status(err.status || 500).json({ message });
});

module.exports = app;
