const { QueryTypes, Op } = require('sequelize');
const models = require('./../models');
const { ChartAccount, MasterTax, sequelize } = models;
const { INV_TYPE } = require('./../helpers/constants');

// PURCHASE INVOICE -- PEMBELIAN
exports.generateJournalEntryLine = async (line) => {
  let arr = [];

  // find masterTaxId detail
  const masterTax = await MasterTax.findOne({
    where: {
      id: line.MasterTax_ID,
    },
  });

  // condition based on TaxLabel in MMasterTax table
  if (masterTax.TaxLabel === 'pph23_non_npwp' || masterTax.TaxLabel === 'pph23_npwp') {
    const coaId = await ChartAccount.findAll({
      where: { AccountNumber: '2201', Business_ID: line.Business_ID },
    });
    arr.push(
      {
        ChartAccount_ID: line.ChartAccount_ID,
        description: line.Name,
        debit: line.TotalAmount,
        credit: 0,
      },
      {
        ChartAccount_ID: coaId[0].id,
        description: coaId[0].AccountName,
        debit: 0,
        credit: line.PphTax,
      }
    );
  } else if (masterTax.TaxLabel === 'ppn_inclusive' || masterTax.TaxLabel === 'ppn_exclusive') {
    const coaId = await ChartAccount.findAll({
      where: { AccountNumber: '2101', Business_ID: line.Business_ID },
    });
    arr.push(
      {
        ChartAccount_ID: line.ChartAccount_ID,
        description: line.name,
        debit: line.TotalAmount,
        credit: 0,
      },
      {
        ChartAccount_ID: coaId[0].id,
        description: coaId[0].AccountName,
        debit: line.PpnTax,
        credit: 0,
      }
    );
  } else if (
    masterTax.TaxLabel === 'pph23_non_npwp_AND_ppn_inclusive' || 
    masterTax.TaxLabel === 'pph23_non_npwp_AND_ppn_exclusive' ||
    masterTax.TaxLabel === 'pph23_npwp_AND_ppn_inclusive' || 
    masterTax.TaxLabel === 'pph23_npwp_AND_ppn_exclusive'
  ) {
    const coaId = await ChartAccount.findAll({
      where: {
        AccountNumber: {
          [Op.in]: ['2101', '2201'],
        },
        Business_ID: line.Business_ID,
      },
    });
    arr.push(
      {
        ChartAccount_ID: line.ChartAccount_ID,
        description: line.name,
        debit: line.TotalAmount,
        credit: 0,
      },
      {
        ChartAccount_ID: coaId[0].id,
        description: coaId[0].AccountName,
        debit: line.PpnTax,
        credit: 0,
      },
      {
        ChartAccount_ID: coaId[1].id,
        description: coaId[1].AccountName,
        debit: 0,
        credit: line.PphTax,
      }
    );
  }

  return arr;
};

exports.sumFromJournalEntryLine = (invLine, key) => {
  if (invLine.length > 0) {
    return invLine.reduce((a, b) => {
      return a + b[key];
    }, 0);
  } else {
    return 0;
  }
};
