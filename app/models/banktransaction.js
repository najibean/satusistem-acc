'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class BankTransaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.BankAccount, {
        foreignKey: 'BankAccount_ID',
        as: 'bank_account'
      })
    }
  }
  BankTransaction.init(
    {
      BankAccount_ID: DataTypes.UUID,
      KodeTransaksi: DataTypes.STRING,
      Description: DataTypes.STRING,
      DateTransaction: DataTypes.DATE,
      Debit: DataTypes.DOUBLE,
      Credit: DataTypes.DOUBLE,
      TransactionStatus: DataTypes.STRING,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'BankTransaction',
      tableName: 'MBankTransaction',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  BankTransaction.beforeCreate((el) => (el.id = uuidv4()));
  return BankTransaction;
};
