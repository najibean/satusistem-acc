'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.changeColumn('MMasterCurrency', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MMasterTax', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MListBank', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MPartner', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MChartAccount', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MJournalEntry', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MBankAccount', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MBankTransaction', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MOrder', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MExpense', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MPayment', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MJournalEntryLine', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MInvoice', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MPaymentLine', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MInvoiceLine', 'Business_ID', {
      type: Sequelize.INTEGER,
    });

    await queryInterface.changeColumn('MOrderLine', 'Business_ID', {
      type: Sequelize.INTEGER,
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.changeColumn('MMasterCurrency', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MMasterTax', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MListBank', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MPartner', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MChartAccount', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MJournalEntry', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MBankAccount', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MBankTransaction', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MOrder', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MExpense', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MPayment', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MJournalEntryLine', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MInvoice', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MPaymentLine', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MInvoiceLine', 'Business_ID', {
      type: Sequelize.STRING,
    });

    await queryInterface.changeColumn('MOrderLine', 'Business_ID', {
      type: Sequelize.STRING,
    });
  },
};
