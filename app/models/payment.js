'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class Payment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.BankAccount, {
        foreignKey: 'BankAccount_ID',
        as: 'bank_account'
      })

      this.belongsTo(models.Partner, {
        foreignKey: 'Partner_ID',
        as: 'partner'
      })

      this.hasMany(models.PaymentLine, {
        foreignKey: 'Payment_ID',
        as: 'payment_line'
      })
    }
  }
  Payment.init(
    {
      PaymentNo: DataTypes.STRING,
      Partner_ID: DataTypes.UUID,
      BankAccount_ID: DataTypes.UUID,
      PaymentType: DataTypes.STRING,
      PaymentMethod: DataTypes.STRING,
      Status: DataTypes.STRING,
      DatePayment: DataTypes.DATE,
      Description: DataTypes.STRING,
      GrandTotal: DataTypes.DOUBLE,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Payment',
      tableName: 'MPayment',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  Payment.beforeCreate((el) => (el.id = uuidv4()));
  return Payment;
};
