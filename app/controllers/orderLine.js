const models = require('../models');
const { OrderLine, MasterTax, sequelize } = models;
const { errorResponse } = require('../helpers/errorResponse');

exports.createOrderLine = async (req, order, order_lines, transaction) => {
  for (const i of order_lines) {
    await OrderLine.create(
      {
        Order_ID: order.id,
        Name: i.Name,
        Description: i.Description,
        Quantity: i.Quantity,
        Price: i.Price,
        PphTax: i.PphTax,
        PpnTax: i.PpnTax,
        MasterTax_ID: i.MasterTax_ID,
        TotalAmount: i.TotalAmount,
        Business_ID: i.Business_ID,
        createdBy: req.user.username,
      },
      { transaction }
    );
  }
};

exports.updateOrderLine = async (req, order, order_lines, transaction) => {
  for (const i of order_lines) {
    await OrderLine.update(
      {
        Order_ID: order.id,
        Name: i.Name,
        Description: i.Description,
        Quantity: i.Quantity,
        Price: i.Price,
        PphTax: i.PphTax,
        PpnTax: i.PpnTax,
        MasterTax_ID: i.MasterTax_ID,
        TotalAmount: i.TotalAmount,
        Business_ID: i.Business_ID,
        updatedBy: req.user.userID,
      },
      {
        where: { id: i.id, Order_ID: order.id },
      },
      { transaction }
    );
  }
};

exports.deleteInvoiceLine = async (req, res, next) => {
  try {
    const { id } = req.params;

    const invLine = await InvoiceLine.findByPk(id);
    if (!invLine) {
      return res.status(404).send(errorResponse('Invoice Line not found'));
    }

    InvoiceLine.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Invoice Line was deleted',
    });
  } catch (error) {
    next(error);
  }
};
