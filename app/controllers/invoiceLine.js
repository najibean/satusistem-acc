const models = require('../models');
const { InvoiceLine, MasterTax, sequelize } = models;
const { errorResponse } = require('../helpers/errorResponse');

exports.createInvoiceLine = async (req, inv, inv_lines, transaction) => {
  for (const i of inv_lines) {
    await InvoiceLine.create(
      {
        Invoice_ID: inv.id,
        ChartAccount_ID: i.ChartAccount_ID,
        Name: i.Name,
        Quantity: i.Quantity,
        Price: i.Price,
        PphTax: i.PphTax,
        PpnTax: i.PpnTax,
        MasterTax_ID: i.MasterTax_ID,
        TotalAmount: i.TotalAmount,
        Business_ID: i.Business_ID,
        createdBy: req.user.username,
      },
      { transaction }
    );
  }
};

exports.updateInvoiceLine = async (req, inv, inv_lines, transaction) => {
  for (const i of inv_lines) {
    await InvoiceLine.update(
      {
        ChartAccount_ID: i.ChartAccount_ID,
        Name: i.Name,
        Quantity: i.Quantity,
        Price: i.Price,
        PphTax: i.PphTax,
        PpnTax: i.PpnTax,
        MasterTax_ID: i.MasterTax_ID,
        TotalAmount: i.TotalAmount,
        Business_ID: i.Business_ID,
        createdBy: req.user.username,
        createdBy: req.user.username,
      },
      {
        where: { id: i.id, Invoice_ID: inv.id },
      },
      { transaction }
    );
  }
};

exports.deleteInvoiceLine = async (req, res, next) => {
  try {
    const { id } = req.params;

    const invLine = await InvoiceLine.findByPk(id);
    if (!invLine) {
      return res.status(404).send(errorResponse('Invoice Line not found'));
    }

    InvoiceLine.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Invoice Line was deleted',
    });
  } catch (error) {
    next(error);
  }
};
