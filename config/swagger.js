'use strict';
require('dotenv').config();
// const JWT = require('jsonwebtoken');

const port = process.env.PORT || '8080';
const baseUrl = process.env.APP_URL || 'localhost';
const url = process.env.NODE_ENV === 'production' ? baseUrl : baseUrl + ':' + port;

module.exports = {
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'Accounting Service Doc API',
      version: '1.0.0',
      description: '',
    },
    components: {
      securitySchemes: {
        ApiKeyAuth: {
          type: 'http',
          // scheme: 'bearer',
          // bearerFormat: JWT,
          type: 'apiKey',
          name: 'Authorization',
          in: 'header',
          description: 'put token here!',
        },
      },
    },
    security: [{ ApiKeyAuth: [] }],
    servers: [
      {
        url: 'http://' + url,
      },
    ],
  },
  apis: [`${__dirname}/../app/routes/*.js`],
};
