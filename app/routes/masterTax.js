const router = require('express').Router();
const { isAuthenticate } = require('../middlewares/authMiddleware');
const {
  createMasterTax,
  getAllMasterTax,
  getMasterTaxById,
  updateMasterTax,
  deleteMasterTax,
  getByBusinessId,
} = require('../controllers/masterTax');

/**
 * @swagger
 * components:
 *  schemas:
 *    MasterTax:
 *      type: object
 *      required:
 *        - TaxLabel
 *        - PphValue
 *        - PpnValue
 *        - Business_ID
 *      properties:
 *        TaxLabel:
 *          type: string
 *          description: Label or name of tax
 *        PphValue:
 *          type: integer
 *          description: Value of PPH
 *        PpnValue:
 *          type: integer
 *          description: Value of PPN
 *        Business_ID:
 *          type: string
 *          description: Bussiness ID of account
 *      example:
 *        TaxLabel: pph_23
 *        PphValue: 0.02
 *        PpnValue: 0.05
 *        Business_ID: Business_ID_001
 *  parameters:
 *    MasterTax_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Master Tax ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/tax/all:
 *  get:
 *    summary: Returns the list of all taxes
 *    tags: [Tax]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the taxes
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/MasterTax'
 */
router.get('/all', getAllMasterTax);

/**
 * @swagger
 * /api/tax/create:
 *  post:
 *    summary: Create new tax
 *    tags: [Tax]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/MasterTax'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Tax was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/MasterTax'
 *      500:
 *        description: Some server error
 */
router.post('/create', createMasterTax);

/**
 * @swagger
 * /api/tax/{id}:
 *  get:
 *    summary: Get tax by id
 *    tags: [Tax]
 *    parameters:
 *      - $ref: '#/components/parameters/MasterTax_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of tax
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/MasterTax'
 *      404:
 *        description: Tax not found
 */
router.get('/:id', getMasterTaxById);

/**
 * @swagger
 * /api/tax/{id}:
 *  patch:
 *    summary: Update tax by the id
 *    tags: [Tax]
 *    parameters:
 *      - $ref: '#/components/parameters/MasterTax_ID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/MasterTax'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Tax was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/MasterTax'
 *      404:
 *        description: Tax not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', updateMasterTax);

/**
 * @swagger
 * /api/tax/{id}:
 *  delete:
 *    summary: Remove tax by id
 *    tags: [Tax]
 *    parameters:
 *      - $ref: '#/components/parameters/MasterTax_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Tax was deleted
 *      404:
 *        description: Tax not found
 */
router.delete('/:id', deleteMasterTax);

/**
 * @swagger
 * /api/tax/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of all taxes based on business id
 *    tags: [Tax]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of all taxes based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/business_id'
 */
router.get('/all/by-business-id/:businessid', getByBusinessId);

module.exports = router;
