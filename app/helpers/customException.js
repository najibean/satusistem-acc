class BadRequestException extends Error {
  constructor(message) {
    super(message);
    this.name = 'BadRequestException';
    this.message = message;
    this.status = 400;
  }
}

class NotFoundException extends Error {
  constructor(message) {
    super(message);
    this.name = 'NotFoundException';
    this.message = message;
    this.status = 404;
  }
}

module.exports = {
  BadRequestException,
  NotFoundException,
};
