'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class MasterCurrency extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.JournalEntry, {
        foreignKey: 'MasterCurrency_ID',
        as: 'journal_entry'
      })

      this.hasMany(models.BankAccount, {
        foreignKey: 'MasterCurrency_ID',
        as: 'bank_account'
      })

      this.hasMany(models.Invoice, {
        foreignKey: 'MasterCurrency_ID',
        as: 'invoice'
      })
    }
  }
  MasterCurrency.init(
    {
      CurrencyLabel: DataTypes.STRING,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'MasterCurrency',
      tableName: 'MMasterCurrency',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  MasterCurrency.beforeCreate((el) => (el.id = uuidv4()));
  return MasterCurrency;
};
