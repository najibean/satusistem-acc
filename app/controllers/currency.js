const models = require('../models');
const { MasterCurrency, sequelize } = models;
const { list, create, update, getById, destroy } = require('./commonController');

exports.createCurrency = async (req, res, next) => {
  const body = {
    CurrencyLabel: req.body.CurrencyLabel,
    Business_ID: req.body.Business_ID,
    createdBy: req.user.username,
  };
  return await create(MasterCurrency, body, 'Currency', res, next);
};

exports.getAllCurrency = async (req, res, next) => {
  return await list(MasterCurrency, req, res, next);
};

exports.getCurrencyById = async (req, res, next) => {
  return await getById(MasterCurrency, req, res, next);
};

exports.updateCurrency = async (req, res, next) => {
  const body = {
    CurrencyLabel: req.body.CurrencyLabel,
    Business_ID: req.body.Business_ID,
  };
  return await update(MasterCurrency, body, 'Currency', req, res, next);
};

exports.deleteCurrency = async (req, res, next) => {
  return await destroy(MasterCurrency, 'Currency', req, res, next);
};
