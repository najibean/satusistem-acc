'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class JournalEntry extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.MasterCurrency, {
        foreignKey: 'MasterCurrency_ID',
        as: 'master_currency'
      })

      this.belongsTo(models.Invoice, {
        foreignKey: 'Invoice_ID',
        as: 'invoice'
      })

      this.hasMany(models.JournalEntryLine, {
        foreignKey: 'JournalEntry_ID',
        as: 'journal_entry_line'
      })
    }
  }
  JournalEntry.init(
    {
      NomorJournal: DataTypes.STRING,
      TanggalJournal: DataTypes.DATE,
      Description: DataTypes.STRING,
      TotalCredit: DataTypes.DOUBLE,
      TotalDebit: DataTypes.DOUBLE,
      Status: DataTypes.STRING,
      MasterCurrency_ID: DataTypes.UUID,
      Invoice_ID: DataTypes.UUID,
      isGenerated: DataTypes.INTEGER,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'JournalEntry',
      tableName: 'MJournalEntry',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  JournalEntry.beforeCreate((el) => (el.id = uuidv4()));
  return JournalEntry;
};
