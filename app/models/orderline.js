'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class OrderLine extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Order, {
        foreignKey: 'Order_ID',
        as: 'order',
      });

      this.belongsTo(models.MasterTax, {
        foreignKey: 'MasterTax_ID',
        as: 'master_tax',
      });
    }
  }
  OrderLine.init(
    {
      Order_ID: DataTypes.UUID,
      Name: DataTypes.STRING,
      Description: DataTypes.STRING,
      Quantity: DataTypes.DOUBLE,
      Price: DataTypes.DOUBLE,
      PphTax: DataTypes.DOUBLE,
      PpnTax: DataTypes.DOUBLE,
      MasterTax_ID: DataTypes.UUID,
      TotalAmount: DataTypes.DOUBLE,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'OrderLine',
      tableName: 'MOrderLine',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  OrderLine.beforeCreate((el) => (el.id = uuidv4()));
  return OrderLine;
};
