'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MOrder', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      OrderNo: {
        type: Sequelize.STRING
      },
      OrderDate: {
        type: Sequelize.DATE
      },
      DeliveryDate: {
        type: Sequelize.DATE
      },
      OrderRef: {
        type: Sequelize.STRING
      },
      Partner_ID: {
        type: Sequelize.UUID
      },
      Subtotal: {
        type: Sequelize.DOUBLE
      },
      Discount: {
        type: Sequelize.DOUBLE
      },
      PphTax: {
        type: Sequelize.DOUBLE
      },
      PpnTax: {
        type: Sequelize.DOUBLE
      },
      GrandTotal: {
        type: Sequelize.DOUBLE
      },
      OrderType: {
        type: Sequelize.STRING
      },
      OrderStatus: {
        type: Sequelize.STRING
      },
      isActive: {
        type: Sequelize.INTEGER
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MOrder', {
      fields: ['Partner_ID'],
      type: 'foreign key',
      name: 'fk_order_partner_id',
      references: {
        table: 'MPartner',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MOrder');
  }
};