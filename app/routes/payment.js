const router = require('express').Router();
const { isAuthenticate } = require('../middlewares/authMiddleware');
const {
  createPayment,
  getAllPayments,
  getPaymentById,
  updatePayment,
  deletePayment,
  getByBusinessId,
} = require('../controllers/payment');
const { validate } = require('../validators');
const { paymentValidator, createPaymentValidator } = require('../validators/paymentValidator');

/**
 * @swagger
 * components:
 *  schemas:
 *    Payment:
 *      type: object
 *      required:
 *        - Partner_ID
 *        - BankAccount_ID
 *        - PaymentType
 *        - PaymentMethod
 *        - Status
 *        - DatePayment
 *        - Description
 *        - isActive
 *        - Business_ID
 *      properties:
 *        Partner_ID:
 *          type: string
 *          description: uuid of partner id
 *        BankAccount_ID:
 *          type: string
 *          description: uuid of bank account id
 *        PaymentType:
 *          type: string
 *          description: type of payment
 *        PaymentMethod:
 *          type: string
 *          description: method of payment
 *        Status:
 *          type: string
 *          description: status of payment
 *        DatePayment:
 *          type: string
 *          description: payment date
 *        Description:
 *          type: string
 *          description: payment description
 *        isActive:
 *          type: integer
 *          description: status active or not by input 1 or 0
 *        Business_ID:
 *          type: string
 *          description: bussiness id
 *      example:
 *        Partner_ID: partner_id
 *        BankAccount_ID: bank_account_id
 *        PaymentType: send
 *        PaymentMethod: bank_transfer
 *        Status: draft
 *        DatePayment: 2022-04-17
 *        Description: percobaan
 *        isActive: 1
 *        Business_ID: Business_ID
 *        payment_invoice:
 *          - invoice_id: invoice_id
 *            amount_paid: 5000
 *  parameters:
 *    Payment_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Chart Account ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/payment/all:
 *  get:
 *    summary: Returns the list of all payments
 *    tags: [Payment]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of payments
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Payment'
 */
router.get('/all', getAllPayments);

/**
 * @swagger
 * /api/payment/create:
 *  post:
 *    summary: Create new payment
 *    tags: [Payment]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Payment'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Payment was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Payment'
 *      500:
 *        description: Some server error
 */
router.post('/create', validate(createPaymentValidator), createPayment);

/**
 * @swagger
 * /api/payment/{id}:
 *  get:
 *    summary: Get payment by id
 *    tags: [Payment]
 *    parameters:
 *      - $ref: '#/components/parameters/Payment_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of payment
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Payment'
 *      404:
 *        description: Partner not found
 */
router.get('/:id', getPaymentById);

/**
 * @swagger
 * /api/payment/{id}:
 *  patch:
 *    summary: Update Payment by the id
 *    tags: [Payment]
 *    parameters:
 *      - $ref: '#/components/parameters/Payment_ID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Payment'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Payment was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Payment'
 *      404:
 *        description: Payment not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', validate(paymentValidator), updatePayment);

/**
 * @swagger
 * /api/payment/{id}:
 *  delete:
 *    summary: Remove Payment by id
 *    tags: [Payment]
 *    parameters:
 *      - $ref: '#/components/parameters/Payment_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Payment was deleted
 *      404:
 *        description: Payment not found
 */
router.delete('/:id', deletePayment);

/**
 * @swagger
 * /api/payment/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of payments based on business id
 *    tags: [Payment]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of payments based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/business_id'
 */
router.get('/all/by-business-id/:businessid', getByBusinessId);

module.exports = router;
