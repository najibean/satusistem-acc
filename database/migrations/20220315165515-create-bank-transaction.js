'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MBankTransaction', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      BankAccount_ID: {
        type: Sequelize.UUID
      },
      KodeTransaksi: {
        type: Sequelize.STRING
      },
      Description: {
        type: Sequelize.STRING
      },
      DateTransaction: {
        type: Sequelize.DATE
      },
      Debit: {
        type: Sequelize.DOUBLE
      },
      Credit: {
        type: Sequelize.DOUBLE
      },
      TransactionStatus: {
        type: Sequelize.STRING
      },
      isActive: {
        type: Sequelize.INTEGER
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MBankTransaction', {
      fields: ['BankAccount_ID'],
      type: 'foreign key',
      name: 'fk_banktransaction_bankaccount_id',
      references: {
        table: 'MBankAccount',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MBankTransaction');
  }
};