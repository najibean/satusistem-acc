const models = require('../models');
const { sequelize } = models;
const { NotFoundException, BadRequestException } = require('./../helpers/customException');

const options = (page, paginate) => {
  return {
    limit: Number(paginate),
    offset: (page - 1) * Number(paginate),
    order: [['updated_at', 'DESC']],
  };
};

const isIdFound = async (model, id) => {
  const idFound = await model.findByPk(id);
  if (!idFound) throw new BadRequestException('Data not found');
};

exports.list = async (model, req, res, next) => {
  try {
    const { page = 1, paginate = 10 } = req.query;

    let docs = await model.findAll(options(page, paginate));

    return res.status(200).send({
      total: docs.length,
      data: docs,
    });
  } catch (err) {
    next(err);
  }
};

exports.getById = async (model, req, res, next) => {
  try {
    const { id } = req.params;

    await isIdFound(model, id);

    let data = await model.findByPk(id);

    if (data.Password) {
      delete data.dataValues.Password;
    }

    return res.status(200).send({
      data,
    });
  } catch (err) {
    next(err);
  }
};

exports.create = async (model, body, modelName, res, next) => {
  try {
    const result = await sequelize.transaction(async () => {
      return await model.create(body, { transaction: t });
    });

    const data = await model.findByPk(result.id);

    if (data.Password) {
      delete data.dataValues.Password;
    }

    return res.status(200).send({
      message: `${modelName} created`,
      data,
    });
  } catch (err) {
    if (err.name == 'SequelizeUniqueConstraintError') {
      return next(err.parent);
    }
    next(err);
  }
};

exports.update = async (model, body, modelName, req, res, next) => {
  try {
    const { id } = req.params;

    await isIdFound(model, id);

    await sequelize.transaction(async () => {
      await model.update(body, { where: { id } }, { transaction: t });
    });

    const data = await model.findOne({
      where: { id },
      attributes: {
        exclude: ['created_at', 'deleted_at'],
      },
    });

    return res.status(200).send({
      message: `${modelName} updated`,
      data,
    });
  } catch (err) {
    next(err);
  }
};

exports.destroy = async (model, modelName, req, res, next) => {
  try {
    const { id } = req.params;

    await isIdFound(model, id);

    await sequelize.transaction(async () => {
      await model.destroy({ where: { id } }, { transaction: t });
    });

    return res.status(200).send({ message: `Success delete ${modelName}` });
  } catch (err) {
    next(err);
  }
};
