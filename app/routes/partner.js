const router = require('express').Router();
const { isAuthenticate } = require('./../middlewares/authMiddleware');
const {
  createPartner,
  getAllPartners,
  getPartnerById,
  updatePartner,
  deletePartner,
  getByBusinessId,
  getCountryPartner,
  getCitiesBaseOnCountry,
  getStateBaseOnCountry,
  getCityBaseOnCountryAndState,
} = require('./../controllers/partner');
const { validate } = require('../validators');
const { partnerValidator, createPartnerValidator } = require('../validators/partnerValidator');

/**
 * @swagger
 * components:
 *  schemas:
 *    Partner:
 *      type: object
 *      required:
 *        - PartnerName
 *        - TaxNo
 *        - Email
 *        - Address
 *        - City
 *        - PhoneNo
 *        - Province
 *        - Zipcode
 *        - PartnerType
 *        - isActive
 *        - Business_ID
 *      properties:
 *        PartnerName:
 *          type: string
 *        TaxNo:
 *          type: string
 *        Email:
 *          type: string
 *        Address:
 *          type: string
 *        City:
 *          type: string
 *        PhoneNo:
 *          type: string
 *        Province:
 *          type: string
 *        Zipcode:
 *          type: string
 *        PartnerType:
 *          type: string
 *        isActive:
 *          type: integer
 *        Business_ID:
 *          type: integer
 *      example:
 *        PartnerName: nama_partner
 *        TaxNo: 325632562
 *        Email: partner@dispostable.com
 *        Address: Jl. Cendrawasih
 *        City: Kota Batam
 *        PhoneNo: 085324390582
 *        Province: Kepulauan Riau
 *        Zipcode: 45173
 *        PartnerType: CLIENT
 *        isActive: 0
 *        Business_ID: 1
 *  parameters:
 *    PartnerID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Partner ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/partner/all:
 *  get:
 *    summary: Returns the list of all partners
 *    tags: [Partner]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the books
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Partner'
 */
router.get('/all', getAllPartners);

/**
 * @swagger
 * /api/partner/create:
 *  post:
 *    summary: Create new partner
 *    tags: [Partner]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Partner'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Partner was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Partner'
 *      500:
 *        description: Some server error
 */
router.post('/create', validate(createPartnerValidator), createPartner);

/**
 * @swagger
 * /api/partner/{id}:
 *  get:
 *    summary: Get partner by id
 *    tags: [Partner]
 *    parameters:
 *      - $ref: '#/components/parameters/PartnerID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of partner
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Partner'
 *      404:
 *        description: Partner not found
 */
router.get('/:id', getPartnerById);

/**
 * @swagger
 * /api/partner/{id}:
 *  patch:
 *    summary: Update partner by the id
 *    tags: [Partner]
 *    parameters:
 *      - $ref: '#/components/parameters/PartnerID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Partner'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The book was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Partner'
 *      404:
 *        description: Partner not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', validate(partnerValidator), updatePartner);

/**
 * @swagger
 * /api/partner/{id}:
 *  delete:
 *    summary: Remove partner by id
 *    tags: [Partner]
 *    parameters:
 *      - $ref: '#/components/parameters/PartnerID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Partner was deleted
 *      404:
 *        description: Partner not found
 */
router.delete('/:id', deletePartner);

/**
 * @swagger
 * /api/partner/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of partners based on business id
 *    tags: [Partner]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of partners based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/business_id'
 */
router.get('/all/by-business-id/:businessid', getByBusinessId);

/**
 * @swagger
 * /api/partner/all/countries:
 *  get:
 *    summary: Returns the list of all countries
 *    tags: [Partner]
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                country:
 *                  type: string 
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of countries
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                type: string
 */
router.get('/all/countries', getCountryPartner);

/**
 * @swagger
 * /api/partner/all/cities:
 *  post:
 *    summary: Returns the list of cities base on country
 *    tags: [Partner]
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                country:
 *                  type: string 
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of cities
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                type: string
 */
router.post('/all/cities', getCitiesBaseOnCountry);

/**
 * @swagger
 * /api/partner/all/states:
 *  post:
 *    summary: Returns the list of states/provinces base on country
 *    tags: [Partner]
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                country:
 *                  type: string 
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of states/provinces
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                type: string
 */
router.post('/all/states', getStateBaseOnCountry);

/**
 * @swagger
 * /api/partner/all/cities-on-state:
 *  post:
 *    summary: Returns the list of cities base on country and states
 *    tags: [Partner]
 *    requestBody:
 *      required: true
 *      content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                country:
 *                  type: string 
 *                state:
 *                  type: string 
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of cities
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                type: string
 */
router.post('/all/cities-on-state', getCityBaseOnCountryAndState);

module.exports = router;
