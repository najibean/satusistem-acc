const router = require('express').Router();
const { isAuthenticate } = require('./../middlewares/authMiddleware');
const {
  createInvoice,
  getAllInvoice,
  getInvoiceById,
  updateInvoice,
  deleteInvoice,
  unpaidInvoices,
  getByBusinessId,
  getInvoiceByPartnerId,
} = require('./../controllers/invoice');
const { deleteInvoiceLine } = require('./../controllers/invoiceLine');
const { validate } = require('./../validators');
const { invoiceValidator, updateInvoiceValidator } = require('./../validators/invoiceValidator');

/**
 * @swagger
 * components:
 *  schemas:
 *    Invoice:
 *      type: object
 *      required:
 *        - InvoiceNo
 *        - InvoiceDate
 *        - DueDate
 *        - Order_ID
 *        - MasterCurrency_ID
 *        - Partner_ID
 *        - Subtotal
 *        - Discount
 *        - PphTax
 *        - PpnTax
 *        - GrandTotal
 *        - InvoiceType
 *        - InvoiceStatus
 *        - isActive
 *        - JournalEntry_ID
 *        - Business_ID
 *      properties:
 *        InvoiceNo:
 *          type: string
 *          description: Number invoice
 *        InvoiceDate:
 *          type: string
 *          description: Invoice date
 *        DueDate:
 *          type: string
 *          description: Due date of invoice
 *        Order_ID:
 *          type: string
 *          description: Id of order
 *        MasterCurrency_ID:
 *          type: string
 *          description: Currency
 *        Partner_ID:
 *          type: string
 *          description: Id of partner
 *        Subtotal:
 *          type: string
 *          description: Subtotal
 *        Discount:
 *          type: string
 *          description: Discount
 *        PphTax:
 *          type: string
 *          description: PPH tax
 *        PpnTax:
 *          type: string
 *          description: PPN tax
 *        GrandTotal:
 *          type: string
 *          description: Grand total
 *        InvoiceType:
 *          type: string
 *          description: Type of invoice
 *        InvoiceStatus:
 *          type: string
 *          description: Status of invoice
 *        isActive:
 *          type: string
 *          description: Status
 *        JournalEntry_ID:
 *          type: string
 *          description: Journal entry ID
 *        Business_ID:
 *          type: string
 *          description: Bussiness ID of invoice
 *        invoice_line:
 *          type: array
 *          description: array of invoice line
 *          items:
 *            type: object
 *            $ref: '#/components/schemas/InvoiceLine'
 *      example:
 *        InvoiceNo: INV_NO_456789
 *        InvoiceDate: 2022-07-13
 *        DueDate: 2022-07-19
 *        Order_ID: 401b1be1-e832-442f-8d2b-ef20866c74a7
 *        MasterCurrency_ID: 5b972b0f-ab0a-4db8-b4ad-08019c6c57a9
 *        Partner_ID: fe16056f-6441-4205-9904-94d81040b396
 *        Subtotal: 270000
 *        Discount: 0.2
 *        PphTax: 200000
 *        PpnTax: 200000
 *        GrandTotal: 787878787
 *        InvoiceType: SALES
 *        InvoiceStatus: PAID
 *        isActive: 1
 *        JournalEntry_ID: a7098f74-138e-49d6-9312-3a1140a56741
 *        Business_ID: Business_ID
 *        invoice_line:
 *          - ChartAccount_ID: ac53e2ad-eefb-42d1-a3e9-348f9bc60ee1
 *            Name: sya
 *            Quantity: 3
 *            Price: 3000
 *            PphTax: 500
 *            PpnTax: 300
 *            MasterTax_ID: 810593e9-bbff-4c0c-8b4f-3ef00ed6dc38
 *            TotalAmount: 7000
 *            Business_ID: Business_ID
 *    InvoiceLine:
 *      type: object
 *      required:
 *        - ChartAccount_ID
 *        - Name
 *        - Quantity
 *        - Price
 *        - PphTax
 *        - PpnTax
 *        - MasterTax_ID
 *        - TotalAmount
 *        - Business_ID
 *      properties:
 *        ChartAccount_ID:
 *          type: string
 *          description: Number invoice
 *        Name:
 *          type: string
 *          description: Number invoice
 *        Quantity:
 *          type: string
 *          description: Number invoice
 *        Price:
 *          type: string
 *          description: Number invoice
 *        PphTax:
 *          type: string
 *          description: Number invoice
 *        PpnTax:
 *          type: string
 *          description: Number invoice
 *        MasterTax_ID:
 *          type: string
 *          description: Number invoice
 *        TotalAmount:
 *          type: string
 *          description: Number invoice
 *        Business_ID:
 *          type: string
 *          description: Number invoice
 *      example:
 *        ChartAccount_ID: ac53e2ad-eefb-42d1-a3e9-348f9bc60ee1
 *        Name: sya
 *        Quantity: 3
 *        Price: 3000
 *        PphTax: 500
 *        PpnTax: 300
 *        MasterTax_ID: 810593e9-bbff-4c0c-8b4f-3ef00ed6dc38
 *        TotalAmount: 7000
 *        Business_ID: Business_ID
 *  parameters:
 *    Invoice_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Invoice ID
 *    InvoiceLine_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Invoice line ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 *    partner_id:
 *      in: path
 *      name: partner_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Partner ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/inv/all:
 *  get:
 *    summary: Returns the list of all invoices
 *    tags: [Invoice]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the invoices
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Invoice'
 */
router.get('/all', getAllInvoice);

/**
 * @swagger
 * /api/inv/create:
 *  post:
 *    summary: Create new Invoice
 *    tags: [Invoice]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Invoice'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Invoice was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *      500:
 *        description: Some server error
 */
router.post('/create', validate(invoiceValidator), createInvoice);

/**
 * @swagger
 * /api/inv/{id}:
 *  get:
 *    summary: Get invoice by id
 *    tags: [Invoice]
 *    parameters:
 *      - $ref: '#/components/parameters/Invoice_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of invoice
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *      404:
 *        description: Partner not found
 */
router.get('/:id', getInvoiceById);

/**
 * @swagger
 * /api/inv/{id}:
 *  patch:
 *    summary: Update Invoice by the id
 *    tags: [Invoice]
 *    parameters:
 *      - $ref: '#/components/parameters/Invoice_ID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Invoice'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Invoice was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Invoice'
 *      404:
 *        description: Invoice not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', validate(updateInvoiceValidator), updateInvoice);

/**
 * @swagger
 * /api/inv/{id}:
 *  delete:
 *    summary: Remove Invoice by id
 *    tags: [Invoice]
 *    parameters:
 *      - $ref: '#/components/parameters/Invoice_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Invoice was deleted
 *      404:
 *        description: Invoice not found
 */
router.delete('/:id', deleteInvoice);

/**
 * @swagger
 * /api/inv/line/{id}:
 *  delete:
 *    summary: Remove Invoice Line by id
 *    tags: [Invoice]
 *    parameters:
 *      - $ref: '#/components/parameters/InvoiceLine_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Invoice Line was deleted
 *      404:
 *        description: Invoice Line not found
 */
router.delete('/line/:id', deleteInvoiceLine);

/**
 * @swagger
 * /api/inv/all/unpaid:
 *  get:
 *    summary: Returns the list of unpaid invoices
 *    tags: [Invoice]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of unpaid invoices
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Invoice'
 */
router.get('/all/unpaid', unpaidInvoices);

/**
 * @swagger
 * /api/inv/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of all invoices based on business id
 *    tags: [Invoice]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the invoices based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Invoice'
 */
router.get('/all/by-business-id/:businessid', getByBusinessId);

/**
 * @swagger
 * /api/inv/all/by-partner-id/{partner_id}:
 *  get:
 *    summary: Returns the list of all invoices based on partner id
 *    tags: [Invoice]
 *    parameters:
 *      - $ref: '#/components/parameters/partner_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the invoices based on partner id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Invoice'
 */
router.get('/all/by-partner-id/:partnerId', getInvoiceByPartnerId);


module.exports = router;
