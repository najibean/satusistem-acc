const router = require('express').Router();
const { isAuthenticate } = require('../middlewares/authMiddleware');
const {
  createOrder,
  deleteOrder,
  getOrderById,
  getAllOrder,
  updateOrder,
  getByBusinessId,
} = require('../controllers/order');
const { validate } = require('../validators');
const { orderValidator } = require('../validators/orderValidator');

/**
 * @swagger
 * components:
 *  schemas:
 *    Order:
 *      type: object
 *      required:
 *        - OrderDate
 *        - DeliveryDate
 *        - OrderRef
 *        - Partner_ID
 *        - Discount
 *        - OrderType
 *        - OrderStatus
 *        - isActive
 *        - Business_ID
 *        - order_line
 *      properties:
 *        OrderDate:
 *          type: string
 *          description: Date of order
 *        DeliveryDate:
 *          type: string
 *          description: Date delivery of order
 *        OrderRef:
 *          type: string
 *          description: Reference of order
 *        Partner_ID:
 *          type: string
 *          description: Partner ID of order
 *        Discount:
 *          type: string
 *          description: Order discount
 *        OrderType:
 *          type: string
 *          description: Type of order
 *        OrderStatus:
 *          type: string
 *          description: Status of order
 *        isActive:
 *          type: string
 *          description: Status order is active or not
 *        Business_ID:
 *          type: string
 *          description: Bussiness ID of order
 *        order_line:
 *          type: array
 *          description: array of order line
 *          items:
 *            type: object
 *            $ref: '#/components/schemas/OrderLine'
 *      example:
 *        id: e63081d5-a095-423b-ac10-394fa28f8d8f
 *        OrderNo: ORD001
 *        OrderDate: 2022-07-13 00:00:00
 *        DeliveryDate: 2022-07-13 00:00:00
 *        OrderRef: ref123
 *        Partner_ID: 675470be-c42a-4698-b971-1cc00818d06c
 *        Subtotal: 9000
 *        Discount: 0
 *        PphTax: 360
 *        PpnTax: 0
 *        GrandTotal: 8640
 *        OrderType: PURCHASE
 *        OrderStatus: DELIVERY
 *        isActive: 1
 *        Business_ID: 5
 *        createdBy: 0ae8df42-1228-48a6-af95-ccbdb70aae19
 *        created_at: 2022-05-24 06:04:18
 *        updated_at: 2022-05-24 06:04:18
 *        deleted_at: null
 *        invoice_line:
 *          - id: 4a84f60d-bfef-422a-be8a-7e58fdf7930d
 *            Order_ID: e63081d5-a095-423b-ac10-394fa28f8d8f
 *            Name: pertamax
 *            Description: salah satu bahan bakar minyak
 *            Quantity: 1
 *            Price: 9000
 *            PphTax: 360
 *            PpnTax: 0
 *            MasterTax_ID: 2e5a0d59-c3b5-431e-8dcd-1475b84dd02f
 *            TotalAmount: 9000
 *            Business_ID: 5
 *            createdBy: 0ae8df42-1228-48a6-af95-ccbdb70aae19
 *            created_at: 2022-05-24 06:04:18
 *            updated_at: 2022-05-24 06:04:18
 *            deleted_at: null
 *    OrderLine:
 *      type: object
 *      required:
 *        - ChartAccount_ID
 *        - Name
 *        - Quantity
 *        - Price
 *        - PphTax
 *        - PpnTax
 *        - MasterTax_ID
 *        - TotalAmount
 *        - Business_ID
 *      properties:
 *        ChartAccount_ID:
 *          type: string
 *          description: Number invoice
 *        Name:
 *          type: string
 *          description: Number invoice
 *        Quantity:
 *          type: string
 *          description: Number invoice
 *        Price:
 *          type: string
 *          description: Number invoice
 *        PphTax:
 *          type: string
 *          description: Number invoice
 *        PpnTax:
 *          type: string
 *          description: Number invoice
 *        MasterTax_ID:
 *          type: string
 *          description: Number invoice
 *        TotalAmount:
 *          type: string
 *          description: Number invoice
 *        Business_ID:
 *          type: string
 *          description: Number invoice
 *      example:
 *        ChartAccount_ID: ac53e2ad-eefb-42d1-a3e9-348f9bc60ee1
 *        Name: sya
 *        Quantity: 3
 *        Price: 3000
 *        PphTax: 500
 *        PpnTax: 300
 *        MasterTax_ID: 810593e9-bbff-4c0c-8b4f-3ef00ed6dc38
 *        TotalAmount: 7000
 *        Business_ID: Business_ID
 *  parameters:
 *    Order_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Order ID
 *    OrderLine_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Order line ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/order/all:
 *  get:
 *    summary: Returns the list of all orders
 *    tags: [Order]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the orders
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Order'
 */
router.get('/all', getAllOrder);

/**
 * @swagger
 * /api/order/create:
 *  post:
 *    summary: Create new Order
 *    tags: [Order]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Order'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Order was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Order'
 *      500:
 *        description: Some server error
 */
router.post('/create', validate(orderValidator), createOrder);

/**
 * @swagger
 * /api/order/{id}:
 *  get:
 *    summary: Get order by id
 *    tags: [Order]
 *    parameters:
 *      - $ref: '#/components/parameters/Order_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of order
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Order'
 *      404:
 *        description: Partner not found
 */
router.get('/:id', getOrderById);

/**
 * @swagger
 * /api/order/{id}:
 *  patch:
 *    summary: Update Order by the id
 *    tags: [Order]
 *    parameters:
 *      - $ref: '#/components/parameters/Order_ID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Order'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Order was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Order'
 *      404:
 *        description: Order not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', validate(orderValidator), updateOrder);

/**
 * @swagger
 * /api/order/{id}:
 *  delete:
 *    summary: Remove Order by id
 *    tags: [Order]
 *    parameters:
 *      - $ref: '#/components/parameters/Order_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Order was deleted
 *      404:
 *        description: Order not found
 */
router.delete('/:id', deleteOrder);

/**
 * @swagger
 * /api/order/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of all orders based on business id
 *    tags: [Order]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the orders based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Order'
 */
router.get('/all/by-business-id/:businessid', getByBusinessId);

module.exports = router;
