const { body } = require('express-validator');

module.exports = [
  body('Business_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isNumeric()
    .withMessage('Must be contain number only')
    .bail()
    .custom(async (value, { req }) => {
      if (typeof value !== 'number') {
        throw new Error('Must be number type');
      }

      return true;
    }),
];
