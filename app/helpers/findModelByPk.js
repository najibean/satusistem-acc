const { BadRequestException } = require('./customException');

module.exports = async (model, modelName, pk) => {
  const dataFound = await model.findOne({
    where: { id: pk },
    attributes: {
      exclude: ['deleted_at'],
    },
  });

  if (!dataFound) {
    throw new BadRequestException(`${modelName} not found`);
  }

  return dataFound;
};
