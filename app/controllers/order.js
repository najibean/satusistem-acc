const models = require('../models');
const { Order, OrderLine, sequelize } = models;
const { Op } = require('sequelize');
const { errorResponse } = require('../helpers/errorResponse');
const { createOrderLine } = require('./orderLine');
const { formatDateLocal } = require('./../helpers/dateConverter');
const { createJournalEntry } = require('./journalEntry');
const { sumFromOrderLine, orderLineHandler } = require('./../repositories/orderRepository');
const { ORDER_STATUS } = require('./../helpers/constants');
const generateNumber = require('./../helpers/generateNumber');

exports.createOrder = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    let {
      OrderDate,
      DeliveryDate,
      OrderRef,
      Partner_ID,
      Discount,
      OrderType,
      OrderStatus,
      isActive,
      Business_ID,
      order_line
    } = req.body;

    const orderLineRepo = await orderLineHandler(order_line);

    const Subtotal = sumFromOrderLine(orderLineRepo, 'TotalAmount');
    const PphTax = sumFromOrderLine(orderLineRepo, 'PphTax');
    const PpnTax = sumFromOrderLine(orderLineRepo, 'PpnTax');
    const GrandTotal = sumFromOrderLine(orderLineRepo, 'GrandTotal');

    const order = await Order.create(
      {
        OrderNo: await generateNumber(Order, 'ORD', 'OrderNo'),
        OrderDate: formatDateLocal(OrderDate),
        DeliveryDate: formatDateLocal(DeliveryDate),
        OrderRef: OrderRef === '' ? null : OrderRef,
        Partner_ID,
        Subtotal,
        Discount,
        PphTax,
        PpnTax,
        GrandTotal,
        OrderType: OrderType.toUpperCase(),
        OrderStatus: ORDER_STATUS.DELIVERY,
        isActive,
        Business_ID,
        createdBy: req.user.username,
      },
      { transaction: t }
    );

    if (order_line.length > 0) {
      await createOrderLine(req, order, orderLineRepo, t);
    }

    // await createJournalEntry(req, order, orderLineRepo, t);

    await t.commit();

    const orderCreated = await Order.findOne({
      where: { id: order.id },
      include: {
        model: OrderLine,
        as: 'order_line',
      },
    });

    return res.status(200).send({
      message: 'Order created',
      data: orderCreated,
    });
  } catch (error) {
    next(error);
  }
};

exports.getAllOrder = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const order = await Order.findAll({
      attributes: {
        exclude: ['updated_at', 'deleted_at'],
      },
      include: {
        model: OrderLine,
        as: 'order_line',
      },
      order: [['updated_at', 'DESC']],
    });

    const data = order.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: order.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};

exports.getOrderById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const order = await Order.findOne({
      where: { id },
      attributes: {
        exclude: ['deleted_at'],
      },
      include: {
        model: OrderLine,
        as: 'order_line',
      },
    });

    if (!order) {
      return res.status(404).send(errorResponse('Order not found'));
    }

    return res.status(200).send({
      data: order,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateOrder = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const { id } = req.params;
    let {
      OrderDate,
      DeliveryDate,
      OrderRef,
      Partner_ID,
      Discount,
      OrderType,
      OrderStatus,
      isActive,
      Business_ID,
      order_line
    } = req.body;

    const order = await Order.findByPk(id);
    if (!order) {
      return res.status(404).send(errorResponse('Order not found'));
    }

    await Order.update(
      {
        OrderDate: formatDateLocal(OrderDate),
        DeliveryDate: formatDateLocal(DeliveryDate),
        OrderRef,
        Partner_ID,
        Subtotal,
        Discount,
        PphTax,
        PpnTax,
        GrandTotal,
        OrderType: OrderType.toUpperCase(),
        OrderStatus: ORDER_STATUS.DELIVERY,
        isActive,
        Business_ID,
        createdBy: req.user.username,
      },
      {
        where: { id },
      },
      { transaction: t }
    );

    if (order_line.length > 0) {
      await updateInvoiceLine(req, order, order_line, t);
    }

    await t.commit();

    return res.status(200).send({
      message: 'Order updated',
      data: await Order.findOne({
        where: { id },
        attributes: {
          exclude: ['updated_at', 'deleted_at'],
        },
        include: {
          model: OrderLine,
          as: 'order_line',
        },
      }),
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteOrder = async (req, res, next) => {
  try {
    const { id } = req.params;

    const order = await Order.findByPk(id);
    if (!order) {
      return res.status(404).send(errorResponse('Order not found'));
    }

    Order.destroy({ where: { id } });
    OrderLine.destroy({ where: { Order_ID: id } });

    return res.status(200).send({
      message: 'Order was deleted',
    });
  } catch (error) {
    next(error);
  }
};

exports.getByBusinessId = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const order = await Order.findAll({
      where: {
        Business_ID: req.params.businessid,
      },
    });

    if (order.length == 0) {
      return res.status(400).send({
        message: `Invoice not found on business id ${req.params.businessid}`,
      });
    }

    const data = order.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: order.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};
