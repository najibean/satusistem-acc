'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MMasterTax', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      TaxLabel: {
        type: Sequelize.STRING
      },
      PphValue: {
        type: Sequelize.DOUBLE
      },
      PpnValue: {
        type: Sequelize.DOUBLE
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MMasterTax');
  }
};