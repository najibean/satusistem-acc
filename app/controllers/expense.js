const models = require('../models');
const { Expense, ListBank, ChartAccount, MasterCurrency, sequelize } = models;
const { errorResponse } = require('../helpers/errorResponse');
const { formatDateLocal } = require('./../helpers/dateConverter');
const { createJournalEntryOnExpense } = require('./journalEntry');

exports.createExpense = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    let {
      BankAccount_ID,
      ExpenseName,
      ExpenseVendor,
      ChartAccount_ID,
      ExpenseDate,
      MasterTax_ID,
      ExpenseNominal,
      ExpenseDescription,
      ExpenseAttachment,
      isActive,
      Business_ID,
    } = req.body;

    const exp = await Expense.create(
      {
        BankAccount_ID,
        ExpenseName,
        ExpenseVendor,
        ChartAccount_ID,
        ExpenseDate: formatDateLocal(ExpenseDate),
        MasterTax_ID: MasterTax_ID ? MasterTax_ID : null,
        ExpenseNominal,
        ExpenseDescription,
        ExpenseAttachment,
        isActive,
        Business_ID,
        createdBy: req.user.username,
      },
      { transaction: t }
    );

    await createJournalEntryOnExpense(req, exp, t);

    await t.commit();

    const expCreated = await Expense.findOne({
      where: { id: exp.id },
    });

    return res.status(200).send({
      message: 'Expense created',
      data: expCreated,
    });
  } catch (error) {
    next(error);
  }
};

exports.getAllExpenses = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const exp = await Expense.findAll({
      attributes: {
        exclude: ['updated_at', 'deleted_at'],
      },
      order: [['updated_at', 'DESC']],
    });

    const data = exp.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: exp.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};

exports.getExpenseById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const exp = await Expense.findOne({
      where: { id },
      attributes: {
        exclude: ['deleted_at'],
      },
    });

    if (!exp) {
      return res.status(404).send(errorResponse('Expense not found'));
    }

    return res.status(200).send({
      data: exp,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateExpense = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const { id } = req.params;
    let {
      BankAccount_ID,
      ExpenseName,
      ExpenseVendor,
      ChartAccount_ID,
      ExpenseDate,
      MasterTax_ID,
      ExpenseNominal,
      ExpenseDescription,
      ExpenseAttachment,
      isActive,
      Business_ID,
    } = req.body;

    const exp = await Expense.findByPk(id);
    if (!exp) {
      return res.status(404).send(errorResponse('Bank Account not found'));
    }

    await Expense.update(
      {
        BankAccount_ID,
        ExpenseName,
        ExpenseVendor,
        ChartAccount_ID,
        ExpenseDate,
        MasterTax_ID: MasterTax_ID ? MasterTax_ID : null,
        ExpenseNominal,
        ExpenseDescription,
        ExpenseAttachment,
        isActive,
        Business_ID,
        createdBy: req.user.username,
      },
      {
        where: { id },
      },
      { transaction: t }
    );
    await t.commit();

    return res.status(200).send({
      message: 'Expense updated',
      data: await Expense.findOne({
        where: { id },
        attributes: {
          exclude: ['updated_at', 'deleted_at'],
        },
      }),
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteExpense = async (req, res, next) => {
  try {
    const { id } = req.params;

    const exp = await Expense.findByPk(id);
    if (!exp) {
      return res.status(404).send(errorResponse('Expense not found'));
    }

    Expense.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Expense was deleted',
    });
  } catch (error) {
    next(error);
  }
};

exports.getByBusinessId = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const exp = await Expense.findAll({
      where: {
        Business_ID: req.params.businessid,
      },
    });

    if (exp.length == 0) {
      return res.status(400).send({
        message: `Expense not found on business id ${req.params.businessid}`,
      });
    }

    const data = exp.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: exp.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};
