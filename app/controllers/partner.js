const models = require('./../models');
const { partnerNumber } = require('./../repositories/partnerRepository');
const { Partner, sequelize } = models;
const { PARTNER_TYPE } = require('./../helpers/constants');
const { amountBasedOnPartnerId } = require('./../repositories/partnerRepository');
const country = require('./../../public/country');
const axios = require('axios');
const pagination = require('./../helpers/pagination');
const { BadRequestException } = require('./../helpers/customException');
const getByBusinessId = require('./../helpers/getByBusinessId');
const findPartnerById = require('./../helpers/findModelByPk');

exports.createPartner = async (req, res, next) => {
  try {
    const body = {
      PartnerName: req.body.PartnerName,
      PartnerNo: await partnerNumber(req.body.PartnerType),
      TaxNo: req.body.TaxNo,
      Email: req.body.Email,
      Address: req.body.Address,
      City: req.body.City,
      PhoneNo: req.body.PhoneNo,
      Province: req.body.Province,
      Zipcode: req.body.Zipcode,
      PartnerType: req.body.PartnerType,
      isActive: req.body.isActive || 1,
      Business_ID: req.body.businessId,
      createdBy: req.user.username,
    };

    if (!Object.values(PARTNER_TYPE).includes(body.PartnerType)) {
      throw new BadRequestException('Please fill PartnerType as CLIENT or MULTI or VENDOR');
    }

    const partner = await sequelize.transaction(async (t) => {
      return await Partner.create(body, { transaction: t });
    });

    const partnerCreated = await findPartnerById(Partner, 'Partner', partner.id);

    return res.status(200).send({
      message: 'Partner created',
      data: partnerCreated,
    });
  } catch (error) {
    next(error);
  }
};

exports.getAllPartners = async (req, res, next) => {
  try {
    const { page, paginate } = req.query;

    const partners = await Partner.findAll({
      ...pagination(page, paginate),
    });

    await Promise.all(
      partners.map(async (el) => {
        const amount = await amountBasedOnPartnerId(el.id);
        el.dataValues['amount'] = amount.amount;
        el.dataValues['outstanding_amount'] = amount.outstanding_amount;
      })
    );

    return res.status(200).send({
      total: partners.length,
      data: partners,
    });
  } catch (error) {
    next(error);
  }
};

exports.getPartnerById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const partner = await findPartnerById(Partner, 'Partner', id);

    const amount = await amountBasedOnPartnerId(partner.id);
    partner.dataValues['amount'] = amount.amount;
    partner.dataValues['outstanding_amount'] = amount.outstanding_amount;

    return res.status(200).send({
      data: partner,
    });
  } catch (error) {
    next(error);
  }
};

exports.updatePartner = async (req, res, next) => {
  try {
    const { id } = req.params;
    const body = {
      PartnerName: req.body.PartnerName,
      TaxNo: req.body.TaxNo,
      Email: req.body.Email,
      Address: req.body.Address,
      City: req.body.City,
      PhoneNo: req.body.PhoneNo,
      Province: req.body.Province,
      Zipcode: req.body.Zipcode,
      PartnerType: req.body.PartnerType,
      isActive: req.body.isActive || 1,
    };

    if (!Object.values(PARTNER_TYPE).includes(body.PartnerType)) {
      throw new BadRequestException('Please fill PartnerType as CLIENT or MULTI or VENDOR');
    }

    const partner = await findPartnerById(Partner, 'Partner', id);

    if (body.Email !== partner.Email) {
      const sameEmailFound = await Partner.findOne({
        where: { Email: body.Email },
      });
      if (sameEmailFound) throw new BadRequestException('This email already exist');
    }

    await sequelize.transaction(async (t) => {
      await Partner.update(
        {
          PartnerNo: partner.PartnerNo,
          ...body,
        },
        {
          where: { id },
        },
        { transaction: t }
      );
    });

    const partnerUpdated = await findPartnerById(Partner, 'Partner', id);

    return res.status(200).send({
      message: 'Partner updated',
      data: partnerUpdated,
    });
  } catch (error) {
    next(error);
  }
};

exports.deletePartner = async (req, res, next) => {
  try {
    const { id } = req.params;

    await findPartnerById(Partner, 'Partner', id);

    await sequelize.transaction(async (t) => {
      await Partner.destroy({ where: { id } }, { transaction: t });
    });

    return res.status(200).send({
      message: 'Success delete Partner',
    });
  } catch (error) {
    next(error);
  }
};

exports.getByBusinessId = async (req, res, next) => {
  return await getByBusinessId(Partner, 'Partner', req, res, next);
};

exports.getCountryPartner = async (req, res, next) => {
  try {
    let result = country.map((el) => el.Name);
    return res.status(200).send(result);
  } catch (err) {
    next(err);
  }
};

exports.getCitiesBaseOnCountry = async (req, res, next) => {
  try {
    const { country } = req.body;
    const cities = await axios.post(`https://countriesnow.space/api/v0.1/countries/cities`, {
      country,
    });

    return res.status(200).send(cities.data.data);
  } catch (err) {
    next(err);
  }
};

exports.getStateBaseOnCountry = async (req, res, next) => {
  try {
    const { country } = req.body;
    const cities = await axios.post(`https://countriesnow.space/api/v0.1/countries/states`, {
      country,
    });

    return res.status(200).send(cities.data.data.states);
  } catch (err) {
    next(err);
  }
};

exports.getCityBaseOnCountryAndState = async (req, res, next) => {
  try {
    const { country, state } = req.body;
    const cities = await axios.post(`https://countriesnow.space/api/v0.1/countries/state/cities`, {
      country,
      state,
    });

    return res.status(200).send(cities.data.data);
  } catch (err) {
    next(err);
  }
};
