const router = require('express').Router();
const { isAuthenticate } = require('../middlewares/authMiddleware');
const {
  createBankAccount,
  getAllBankAccounts,
  getBankAccountById,
  updateBankAccount,
  deleteBankAccount,
  listBanks,
  getByBusinessId,
} = require('../controllers/bankAccount');
const { validate } = require('./../validators');
const {
  bankAccountValidator,
  createBankAccountValidator,
} = require('./../validators/bankAccountValidator');

/**
 * @swagger
 * components:
 *  schemas:
 *    BankAccount:
 *      type: object
 *      required:
 *        - ListBank_ID
 *        - NamaAccount
 *        - NoAccount
 *        - BranchName
 *        - BranchCity
 *        - ChartAccount_ID
 *        - MasterCurrency_ID
 *        - Business_ID
 *      properties:
 *        ListBank_ID:
 *          type: string
 *          description: uuid from list bank
 *        NamaAccount:
 *          type: string
 *          description: Name of account
 *        NoAccount:
 *          type: integer
 *          description: Number of account
 *        BranchName:
 *          type: string
 *          description: Name of branch
 *        BranchCity:
 *          type: string
 *          description: Name of city branch
 *        ChartAccount_ID:
 *          type: string
 *          description: uuid from chart account
 *        MasterCurrency_ID:
 *          type: string
 *          description: uuid from master currency
 *        Business_ID:
 *          type: string
 *          description: bussiness id
 *      example:
 *        ListBank_ID: list-bank-id
 *        NamaAccount: nama_account
 *        NoAccount: 12
 *        BranchName: Nagoya
 *        BranchCity: Batam
 *        ChartAccount_ID: chart-acc-id
 *        MasterCurrency_ID: master-curr-id
 *        Business_ID: Business_ID
 *  parameters:
 *    BankAccount_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Chart Account ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/bank-account/all:
 *  get:
 *    summary: Returns the list of all bank accounts
 *    tags: [BankAccount]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the bank accounts
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/BankAccount'
 */
router.get('/all', getAllBankAccounts);

/**
 * @swagger
 * /api/bank-account/create:
 *  post:
 *    summary: Create new bank account
 *    tags: [BankAccount]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/BankAccount'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Bank account was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/BankAccount'
 *      500:
 *        description: Some server error
 */
router.post('/create', validate(createBankAccountValidator), createBankAccount);

/**
 * @swagger
 * /api/bank-account/{id}:
 *  get:
 *    summary: Get bank account by id
 *    tags: [BankAccount]
 *    parameters:
 *      - $ref: '#/components/parameters/BankAccount_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of bank account
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/BankAccount'
 *      404:
 *        description: Partner not found
 */
router.get('/:id', getBankAccountById);

/**
 * @swagger
 * /api/bank-account/{id}:
 *  patch:
 *    summary: Update bank account by the id
 *    tags: [BankAccount]
 *    parameters:
 *      - $ref: '#/components/parameters/BankAccount_ID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/BankAccount'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Bank Account was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/BankAccount'
 *      404:
 *        description: Bank Account not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', validate(bankAccountValidator), updateBankAccount);

/**
 * @swagger
 * /api/bank-account/{id}:
 *  delete:
 *    summary: Remove Bank Account by id
 *    tags: [BankAccount]
 *    parameters:
 *      - $ref: '#/components/parameters/BankAccount_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Bank Account was deleted
 *      404:
 *        description: Bank Account not found
 */
router.delete('/:id', deleteBankAccount);

/**
 * @swagger
 * /api/bank-account/all/banks:
 *  get:
 *    summary: Returns the list of bank
 *    tags: [BankAccount]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of bank
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/BankAccount'
 */
router.get('/all/banks', listBanks);

/**
 * @swagger
 * /api/bank-account/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of all bank_accounts based on business id
 *    tags: [BankAccount]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the bank_accounts based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/BankAccount'
 */
router.get('/all/by-business-id/:businessid', getByBusinessId);

module.exports = router;
