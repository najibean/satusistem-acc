'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MInvoice', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      InvoiceNo: {
        type: Sequelize.STRING
      },
      InvoiceDate: {
        type: Sequelize.DATE
      },
      DueDate: {
        type: Sequelize.DATE
      },
      Order_ID: {
        type: Sequelize.UUID
      },
      MasterCurrency_ID: {
        type: Sequelize.UUID
      },
      Partner_ID: {
        type: Sequelize.UUID
      },
      Subtotal: {
        type: Sequelize.DOUBLE
      },
      Discount: {
        type: Sequelize.DOUBLE
      },
      PphTax: {
        type: Sequelize.DOUBLE
      },
      PpnTax: {
        type: Sequelize.DOUBLE
      },
      GrandTotal: {
        type: Sequelize.DOUBLE
      },
      InvoiceType: {
        type: Sequelize.STRING
      },
      InvoiceStatus: {
        type: Sequelize.STRING
      },
      isActive: {
        type: Sequelize.INTEGER
      },
      JournalEntry_ID: {
        type: Sequelize.UUID
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MInvoice', {
      fields: ['Order_ID'],
      type: 'foreign key',
      name: 'fk_invoice_order_id',
      references: {
        table: 'MOrder',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MInvoice', {
      fields: ['MasterCurrency_ID'],
      type: 'foreign key',
      name: 'fk_invoice_mastercurrency_id',
      references: {
        table: 'MMasterCurrency',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MInvoice', {
      fields: ['Partner_ID'],
      type: 'foreign key',
      name: 'fk_invoice_partner_id',
      references: {
        table: 'MPartner',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MInvoice', {
      fields: ['JournalEntry_ID'],
      type: 'foreign key',
      name: 'fk_invoice_journalentry_id',
      references: {
        table: 'MJournalEntry',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MInvoice');
  }
};