'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MPaymentLine', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      Payment_ID: {
        type: Sequelize.UUID
      },
      Invoice_ID: {
        type: Sequelize.UUID
      },
      Amount: {
        type: Sequelize.DOUBLE
      },
      isActive: {
        type: Sequelize.INTEGER
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MPaymentLine', {
      fields: ['Payment_ID'],
      type: 'foreign key',
      name: 'fk_paymentline_payment_id',
      references: {
        table: 'MPayment',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MPaymentLine', {
      fields: ['Invoice_ID'],
      type: 'foreign key',
      name: 'fk_paymentline_invoice_id',
      references: {
        table: 'MInvoice',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MPaymentLine');
  }
};