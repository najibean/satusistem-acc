'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MPayment', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      PaymentNo: {
        type: Sequelize.STRING
      },
      Partner_ID: {
        type: Sequelize.UUID
      },
      BankAccount_ID: {
        type: Sequelize.UUID
      },
      PaymentType: {
        type: Sequelize.STRING
      },
      PaymentMethod: {
        type: Sequelize.STRING
      },
      Status: {
        type: Sequelize.STRING
      },
      DatePayment: {
        type: Sequelize.DATE
      },
      Description: {
        type: Sequelize.STRING
      },
      GrandTotal: {
        type: Sequelize.DOUBLE
      },
      isActive: {
        type: Sequelize.INTEGER
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MPayment', {
      fields: ['Partner_ID'],
      type: 'foreign key',
      name: 'fk_payment_partner_id',
      references: {
        table: 'MPartner',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MPayment', {
      fields: ['BankAccount_ID'],
      type: 'foreign key',
      name: 'fk_payment_bankaccount_id',
      references: {
        table: 'MBankAccount',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MPayment');
  }
};