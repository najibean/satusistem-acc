const router = require('express').Router();
const partner = require('./partner');
const coa = require('./chartAccount');
const invoice = require('./invoice');
const tax = require('./masterTax');
const bankAccount = require('./bankAccount');
const payment = require('./payment');
const bankTransaction = require('./bankTransaction');
const order = require('./order');
const expense = require('./expense');
const currency = require('./currency');

router.get('/test', (req, res) => {
  res.send('it works!');
});

router.use('/partner', partner);
router.use('/coa', coa);
router.use('/inv', invoice);
router.use('/tax', tax);
router.use('/bank-account', bankAccount);
router.use('/payment', payment);
router.use('/bank-transaction', bankTransaction);
router.use('/order', order);
router.use('/expense', expense);
router.use('/currency', currency);

module.exports = router;

/**
 * @swagger
 * tags:
 *  name: Partner
 *  description: Partner managing API
 */

/**
 * @swagger
 * tags:
 *  name: ChartAccount
 *  description: Chart Account managing API
 */

/**
 * @swagger
 * tags:
 *  name: Invoice
 *  description: Invoice managing API
 */

/**
 * @swagger
 * tags:
 *  name: Tax
 *  description: Tax managing API
 */

/**
 * @swagger
 * tags:
 *  name: BankAccount
 *  description: Bank Account managing API
 */

/**
 * @swagger
 * tags:
 *  name: Payment
 *  description: Payment managing API
 */

/**
 * @swagger
 * tags:
 *  name: BankTransaction
 *  description: Bank Transaction managing API
 */

/**
 * @swagger
 * tags:
 *  name: Order
 *  description: Order managing API
 */

/**
 * @swagger
 * tags:
 *  name: Expense
 *  description: Expense managing API
 */

/**
 * @swagger
 * tags:
 *  name: Currency
 *  description: Currency managing API
 */
