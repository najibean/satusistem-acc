const router = require('express').Router();
const { isAuthenticate } = require('../middlewares/authMiddleware');
const {
  createExpense,
  getAllExpenses,
  getExpenseById,
  updateExpense,
  deleteExpense,
  getByBusinessId
} = require('../controllers/expense');
const { validate } = require('../validators');
const { expenseValidator } = require('../validators/expenseValidator');

/**
 * @swagger
 * components:
 *  schemas:
 *    Expense:
 *      type: object
 *      required:
 *        - BankAccount_ID
 *        - ExpenseName
 *        - ExpenseVendor
 *        - ChartAccount_ID
 *        - ExpenseDate
 *        - MasterTax_ID
 *        - ExpenseNominal
 *        - ExpenseDescription
 *        - ExpenseAttachment
 *        - isActive
 *        - Business_ID
 *      properties:
 *        BankAccount_ID:
 *          type: string
 *          description: uuid of Bank Account id
 *        ExpenseName:
 *          type: string
 *          description: expense name
 *        ExpenseVendor:
 *          type: string
 *          description: expense vendor
 *        ChartAccount_ID:
 *          type: string
 *          description: uuid of chart account
 *        ExpenseDate:
 *          type: string
 *          description: expense date
 *        MasterTax_ID:
 *          type: string
 *          description: uuid of master tax
 *        ExpenseNominal:
 *          type: double
 *          description: expense nominal
 *        ExpenseDescription:
 *          type: string
 *          description: expense description
 *        ExpenseAttachment:
 *          type: file
 *          description: expense attachment
 *        isActive:
 *          type: integer
 *          description: status active or not by input 1 or 0
 *        Business_ID:
 *          type: string
 *          description: bussiness id
 *      example:
 *        BankAccount_ID: 460401be-f1b9-468a-8c24-73fca324400d
 *        ExpenseName: coba expense
 *        ExpenseVendor: PT Partner Sejati
 *        ChartAccount_ID: 8e7dd154-ee37-4ab7-ad35-3fba0c7959a3
 *        ExpenseDate: 2022-05-30
 *        MasterTax_ID: 8e7dd154-ee37-4ab7-ad35-3fba0c7959b5
 *        ExpenseNominal: 3000,
 *        ExpenseDescription: some kind of description
 *        ExpenseAttachment: --file--
 *        isActive: 1,
 *        Business_ID: 
 *  parameters:
 *    Expense_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Expense Account ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/expense/all:
 *  get:
 *    summary: Returns the list of all expenses
 *    tags: [Expense]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of expenses
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Expense'
 */
router.get('/all', getAllExpenses);

/**
 * @swagger
 * /api/expense/create:
 *  post:
 *    summary: Create new expense
 *    tags: [Expense]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Expense'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Expense was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Expense'
 *      500:
 *        description: Some server error
 */
router.post('/create', validate(expenseValidator), createExpense);

/**
 * @swagger
 * /api/expense/{id}:
 *  get:
 *    summary: Get expense by id
 *    tags: [Expense]
 *    parameters:
 *      - $ref: '#/components/parameters/Expense_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of expense
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Expense'
 *      404:
 *        description: Partner not found
 */
router.get('/:id', getExpenseById);

/**
 * @swagger
 * /api/expense/{id}:
 *  patch:
 *    summary: Update Expense by the id
 *    tags: [Expense]
 *    parameters:
 *      - $ref: '#/components/parameters/Expense_ID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Expense'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Expense was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Expense'
 *      404:
 *        description: Expense not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', validate(expenseValidator), updateExpense);

/**
 * @swagger
 * /api/expense/{id}:
 *  delete:
 *    summary: Remove Expense by id
 *    tags: [Expense]
 *    parameters:
 *      - $ref: '#/components/parameters/Expense_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Expense was deleted
 *      404:
 *        description: Expense not found
 */
router.delete('/:id', deleteExpense);

/**
 * @swagger
 * /api/expense/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of expenses based on business id
 *    tags: [Expense]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of expenses based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/business_id'
 */
router.get('/all/by-business-id/:businessid', getByBusinessId);

module.exports = router;
