'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class MasterTax extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Expense, {
        foreignKey: 'MasterTax_ID',
        as: 'expense'
      })

      this.hasMany(models.InvoiceLine, {
        foreignKey: 'MasterTax_ID',
        as: 'invoice_line'
      })
    }
  }
  MasterTax.init(
    {
      TaxLabel: DataTypes.STRING,
      PphValue: DataTypes.DOUBLE,
      PpnValue: DataTypes.DOUBLE,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'MasterTax',
      tableName: 'MMasterTax',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  MasterTax.beforeCreate((el) => (el.id = uuidv4()));
  return MasterTax;
};
