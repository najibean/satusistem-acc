const models = require('../models');
const { PaymentLine, sequelize } = models;
const { errorResponse } = require('../helpers/errorResponse');

exports.createPaymentLine = async (req, pay, payment_invoices, t) => {
  await Promise.all(
    payment_invoices.map(async (el) => {
      await PaymentLine.create(
        {
          Payment_ID: pay.id,
          Invoice_ID: el.Invoice_ID,
          Amount: el.Amount,
          isActive: pay.isActive,
          Business_ID: pay.Business_ID,
          createdBy: req.user.username,
        },
        { transaction: t }
      );
    })
  );
};

exports.updatePaymentLine = async (req, pay, payment_invoices, transaction) => {
  for (const i of payment_invoices) {
    await PaymentLine.update(
      {
        Payment_ID: pay.id,
        Invoice_ID: i.invoice_id,
        Amount: i.Amount,
        isActive: pay.isActive,
        Business_ID: pay.Business_ID,
        createdBy: req.user.username,
      },
      {
        where: { Invoice_ID: i.invoice_id },
      },
      { transaction }
    );
  }
};

exports.deletePaymentLine = async (req, res, next) => {
  try {
    const { id } = req.params;

    const payLine = await PaymentLine.findByPk(id);
    if (!payLine) {
      return res.status(404).send(errorResponse('Payment Line not found'));
    }

    PaymentLine.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Payment Line was deleted',
    });
  } catch (error) {
    next(error);
  }
};
