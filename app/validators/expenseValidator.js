const { body } = require('express-validator');
const models = require('../models');
const { MasterTax, ChartAccount, BankAccount, sequelize } = models;

const expenseValidator = [
  body('BankAccount_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .custom(async (value, { req }) => {
      const ba = await BankAccount.findByPk(value);

      if (!ba) {
        throw new Error('BankAccount_ID not found');
      }

      return true;
    }),
  body('ExpenseName')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('ExpenseVendor')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('ChartAccount_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .custom(async (value, { req }) => {
      const ca = await ChartAccount.findByPk(value);

      if (!ca) {
        throw new Error('ChartAccount_ID not found');
      }

      return true;
    }),
  body('ExpenseDate')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isISO8601()
    .toDate()
    .withMessage('Please use the correct date format (YYYY-MM-DD)'),
  body('MasterTax_ID')
    .optional({ checkFalsy: true })
    .bail()
    .custom(async (value, { req }) => {
      const tax = await MasterTax.findByPk(value);

      if (!tax) {
        throw new Error('MasterTax_ID not found');
      }

      return true;
    }),
  body('ExpenseNominal')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isNumeric()
    .withMessage('Must be contain number only'),
  body('ExpenseDescription')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('Business_ID')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
];

module.exports = { expenseValidator };
