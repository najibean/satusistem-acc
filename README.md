# Accounting Services

## Usage

Basic command

```bash
npm run dev
```

#### Config

Set up in .env files:

```bash
DEV_DB_USERNAME=database_username
DEV_DB_PASSWORD=database_password
DEV_DB_DATABASE=name_of_database
DEV_DB_HOST=host_server
DEV_DB_PORT=3306
DEV_DB_DIALECT=mysql
```
