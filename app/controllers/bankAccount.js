const models = require('../models');
const { BankAccount, ListBank, ChartAccount, MasterCurrency, sequelize } = models;
const { accountNumberHandler } = require('./../repositories/bankAccountRepository');
const pagination = require('./../helpers/pagination');
const getByBusinessId = require('./../helpers/getByBusinessId');
const { BadRequestException } = require('./../helpers/customException');
const findBankAccById = require('./../helpers/findModelByPk');

exports.createBankAccount = async (req, res, next) => {
  try {
    const body = {
      ListBank_ID: req.body.ListBank_ID,
      NamaAccount: req.body.NamaAccount,
      NoAccount: req.body.NoAccount,
      BranchName: req.body.BranchName,
      BranchCity: req.body.BranchCity,
      MasterCurrency_ID: req.body.MasterCurrency_ID,
      Business_ID: req.body.Business_ID,
      createdBy: req.user.username,
    };

    const { CurrencyLabel } = await MasterCurrency.findByPk(body.MasterCurrency_ID);

    const baCreated = await sequelize.transaction(async (t) => {
      const coaCreated = await ChartAccount.create(
        {
          AccountName: `${body.NamaAccount} (${body.NoAccount} - ${CurrencyLabel})`,
          AccountNumber: await accountNumberHandler(body.Business_ID),
          HeaderID: '1100',
          AccountType: 'SSH',
          Business_ID: body.Business_ID,
          createdBy: req.user.username,
        },
        { transaction: t }
      );

      return await BankAccount.create(
        {
          ...body,
          Saldo: 0,
          ChartAccount_ID: coaCreated.id,
        },
        { transaction: t }
      );
    });

    const baFound = await findBankAccById(BankAccount, 'Bank Account', baCreated.id);

    return res.status(200).send({
      message: 'Bank Account created',
      data: baFound,
    });
  } catch (error) {
    next(error);
  }
};

exports.getAllBankAccounts = async (req, res, next) => {
  try {
    const { page, paginate } = req.query;

    const datas = await BankAccount.findAll({
      ...pagination(page, paginate),
    });

    return res.status(200).send({
      total: datas.length,
      data: datas,
    });
  } catch (error) {
    next(error);
  }
};

exports.getBankAccountById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const ba = await findBankAccById(BankAccount, 'Bank Account', id);

    return res.status(200).send({
      data: ba,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateBankAccount = async (req, res, next) => {
  try {
    const { id } = req.params;
    const body = {
      ListBank_ID: req.body.ListBank_ID,
      NamaAccount: req.body.NamaAccount,
      NoAccount: req.body.NoAccount,
      BranchName: req.body.BranchName,
      BranchCity: req.body.BranchCity,
      ChartAccount_ID: req.body.ChartAccount_ID,
      MasterCurrency_ID: req.body.MasterCurrency_ID,
    };

    await findBankAccById(BankAccount, 'Bank Account', id);

    await sequelize.transaction(async (t) => {
      await BankAccount.update(
        body,
        {
          where: { id },
        },
        { transaction: t }
      );
    });

    const baUpdated = await findBankAccById(BankAccount, 'Bank Account', id);

    return res.status(200).send({
      message: 'Bank Account updated',
      data: baUpdated,
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteBankAccount = async (req, res, next) => {
  try {
    const { id } = req.params;

    await findBankAccById(BankAccount, 'Bank Account', id);

    await sequelize.transaction(async (t) => {
      await BankAccount.destroy({ where: { id } });
    });

    return res.status(200).send({
      message: 'Bank Account was deleted',
    });
  } catch (error) {
    next(error);
  }
};

exports.listBanks = async (req, res, next) => {
  try {
    const { page, paginate } = req.query;

    const banks = await ListBank.findAll({
      ...pagination(page, paginate),
    });

    return res.status(200).send({
      total: banks.length,
      data: banks,
    });
  } catch (error) {
    next(error);
  }
};

exports.getByBusinessId = async (req, res, next) => {
  return await getByBusinessId(BankAccount, 'Bank Account', req, res, next);
};
