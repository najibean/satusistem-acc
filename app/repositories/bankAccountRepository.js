const { QueryTypes } = require('sequelize');
const models = require('./../models');
const { sequelize } = models;

const seqNumb = (journalNumb) => {
  return String(Number(journalNumb.slice(1)) + 1).padStart(3, 0);
};

exports.accountNumberHandler = async (businessId) => {
  const queryResult = await sequelize.query(
    `
    SELECT ca.AccountName, ca.AccountNumber
    FROM MChartAccount ca
    WHERE ca.AccountNumber LIKE '8%'
      AND ca.Business_ID = '${businessId}'
    ORDER BY ca.AccountNumber ASC
    `,
    { type: QueryTypes.SELECT }
  );

  if (queryResult.length > 0) {
    const lastAccountNumb = queryResult[queryResult.length - 1].AccountNumber;
    return '8' + seqNumb(lastAccountNumb);
  } else {
    return `8000`;
  }
};
