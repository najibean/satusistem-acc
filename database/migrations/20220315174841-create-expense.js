'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MExpense', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      BankAccount_ID: {
        type: Sequelize.UUID
      },
      ExpenseName: {
        type: Sequelize.STRING
      },
      ExpenseVendor: {
        type: Sequelize.STRING
      },
      ChartAccount_ID: {
        type: Sequelize.UUID
      },
      ExpenseDate: {
        type: Sequelize.DATE
      },
      MasterTax_ID: {
        type: Sequelize.UUID
      },
      ExpenseNominal: {
        type: Sequelize.DOUBLE
      },
      ExpenseDescription: {
        type: Sequelize.STRING
      },
      ExpenseAttachment: {
        type: Sequelize.STRING
      },
      isActive: {
        type: Sequelize.INTEGER
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MExpense', {
      fields: ['BankAccount_ID'],
      type: 'foreign key',
      name: 'fk_expense_bankaccount_id',
      references: {
        table: 'MBankAccount',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MExpense', {
      fields: ['ChartAccount_ID'],
      type: 'foreign key',
      name: 'fk_expense_chartaccount_id',
      references: {
        table: 'MChartAccount',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MExpense', {
      fields: ['MasterTax_ID'],
      type: 'foreign key',
      name: 'fk_expense_mastertax_id',
      references: {
        table: 'MMasterTax',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MExpense');
  }
};