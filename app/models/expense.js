'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class Expense extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.BankAccount, {
        foreignKey: 'BankAccount_ID',
        as: 'bank_account'
      })

      this.belongsTo(models.ChartAccount, {
        foreignKey: 'ChartAccount_ID',
        as: 'chart_account'
      })

      this.belongsTo(models.MasterTax, {
        foreignKey: 'MasterTax_ID',
        as: 'master_tax'
      })
    }
  }
  Expense.init(
    {
      BankAccount_ID: DataTypes.UUID,
      ExpenseName: DataTypes.STRING,
      ExpenseVendor: DataTypes.STRING,
      ChartAccount_ID: DataTypes.UUID,
      ExpenseDate: DataTypes.DATE,
      MasterTax_ID: DataTypes.UUID,
      ExpenseNominal: DataTypes.DOUBLE,
      ExpenseDescription: DataTypes.STRING,
      ExpenseAttachment: DataTypes.STRING,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Expense',
      tableName: 'MExpense',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  Expense.beforeCreate((el) => (el.id = uuidv4()));
  return Expense;
};
