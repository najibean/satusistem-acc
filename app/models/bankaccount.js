'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class BankAccount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Payment, {
        foreignKey: 'BankAccount_ID',
        as: 'payment'
      })

      this.hasMany(models.BankTransaction, {
        foreignKey: 'BankAccount_ID',
        as: 'bank_transaction'
      })

      this.hasMany(models.Expense, {
        foreignKey: 'BankAccount_ID',
        as: 'expense'
      })

      this.belongsTo(models.ChartAccount, {
        foreignKey: 'ChartAccount_ID',
        as: 'chart_account'
      })

      this.belongsTo(models.ListBank, {
        foreignKey: 'ListBank_ID',
        as: 'list_bank'
      })

      this.belongsTo(models.MasterCurrency, {
        foreignKey: 'MasterCurrency_ID',
        as: 'master_currency'
      })
    }
  }
  BankAccount.init(
    {
      ListBank_ID: DataTypes.UUID,
      NamaAccount: DataTypes.STRING,
      NoAccount: DataTypes.INTEGER,
      BranchName: DataTypes.STRING,
      BranchCity: DataTypes.STRING,
      Saldo: DataTypes.DOUBLE,
      ChartAccount_ID: DataTypes.UUID,
      MasterCurrency_ID: DataTypes.UUID,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'BankAccount',
      tableName: 'MBankAccount',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  BankAccount.beforeCreate((el) => (el.id = uuidv4()));
  return BankAccount;
};
