'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class ChartAccount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.BankAccount, {
        foreignKey: 'ChartAccount_ID',
        as: 'bank_account',
      });

      this.hasMany(models.InvoiceLine, {
        foreignKey: 'ChartAccount_ID',
        as: 'invoice_line',
      });

      this.hasMany(models.Expense, {
        foreignKey: 'ChartAccount_ID',
        as: 'expense',
      });

      this.hasMany(models.JournalEntryLine, {
        foreignKey: 'ChartAccount_ID',
        as: 'journal_entry_line',
      });
    }
  }
  ChartAccount.init(
    {
      AccountName: DataTypes.STRING,
      AccountNumber: DataTypes.STRING,
      HeaderID: DataTypes.STRING,
      AccountType: DataTypes.STRING,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'ChartAccount',
      tableName: 'MChartAccount',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  ChartAccount.beforeCreate((el) => (el.id = uuidv4()));
  return ChartAccount;
};
