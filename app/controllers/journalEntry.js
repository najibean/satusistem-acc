const models = require('../models');
const { JournalEntry, Invoice, sequelize } = models;
const { errorResponse } = require('../helpers/errorResponse');
const { formatDateLocal, formatDate } = require('./../helpers/dateConverter');
const { journalEntryDescription } = require('./../repositories/journalEntryRepository');
const generateNumber = require('./../helpers/generateNumber');
const { createBankTransaction } = require('./bankTransaction');
const {
  createJournalEntryLine,
  createJournalEntryLineAfterPayment,
} = require('./journalEntryLine');

exports.createJournalEntry = async (req, inv, inv_lines, transaction) => {
  const journalEntry = await JournalEntry.create(
    {
      NomorJournal: await generateNumber(JournalEntry, 'JRN', 'NomorJournal'),
      TanggalJournal: new Date(),
      Description: journalEntryDescription(inv.InvoiceType, inv.InvoiceNo),
      TotalCredit: inv.GrandTotal,
      TotalDebit: inv.GrandTotal,
      Status: 'POSTED', //[todo] -- value disini berdasarkan apa?
      MasterCurrency_ID: inv.MasterCurrency_ID,
      Invoice_ID: inv.id,
      isGenerated: 1, //[todo] -- value disini berdasarkan apa?
      isActive: inv.isActive,
      Business_ID: inv.Business_ID,
      createdBy: req.user.username,
    },
    { transaction }
  );

  await createJournalEntryLine(req, inv, journalEntry, inv_lines, transaction);
};

exports.createJournalEntryAfterPayment = async (req, payment, paymentLines, t) => {
  await Promise.all(
    paymentLines.map(async (el) => {
      const journalEntry = await JournalEntry.create(
        {
          NomorJournal: await generateNumber(JournalEntry, 'JRN', 'NomorJournal'),
          TanggalJournal: new Date(),
          Description: `Payment untuk invoice No. ${el.invoice.id}`,
          TotalCredit: payment.GrandTotal,
          TotalDebit: payment.GrandTotal,
          Status: 'POSTED', //[todo] -- value disini berdasarkan apa?
          MasterCurrency_ID: el.invoice.MasterCurrency_ID,
          Invoice_ID: el.invoice.id,
          isGenerated: 1, //[todo] -- value disini berdasarkan apa?
          isActive: payment.isActive,
          Business_ID: payment.Business_ID,
          createdBy: req.user.username,
        },
        { transaction: t }
      );

      await createJournalEntryLineAfterPayment(req, payment, journalEntry, paymentLines, t);

      await createBankTransaction(payment, el.invoice, t);
    })
  );
};

exports.createJournalEntryOnExpense = async (req, exp, transaction) => {
  await JournalEntry.create(
    {
      NomorJournal: await generateNumber(JournalEntry, 'JRN', 'NomorJournal'),
      TanggalJournal: new Date(),
      Description: `Create expense for ${exp.ExpenseName}`,
      TotalCredit: null,
      TotalDebit: exp.GrandTotal,
      Status: 'POSTED', //[todo] -- value disini berdasarkan apa?
      MasterCurrency_ID: null,
      Invoice_ID: null,
      isGenerated: 1, //[todo] -- value disini berdasarkan apa?
      isActive: exp.isActive,
      Business_ID: exp.Business_ID,
      createdBy: req.user.username,
    },
    { transaction }
  );

  // await createJournalEntryLine(req, inv, journalEntry, inv_lines, transaction);
};

exports.updateJournalEntry = async (req, inv, inv_lines, transaction) => {
  for (const i of inv_lines) {
    await JournalEntry.update(
      {
        ChartAccount_ID: i.ChartAccount_ID,
        Name: i.Name,
        Quantity: i.Quantity,
        Price: i.Price,
        PphTax: i.PphTax,
        PpnTax: i.PpnTax,
        MasterTax_ID: i.MasterTax_ID,
        TotalAmount: i.TotalAmount,
        Business_ID: i.Business_ID,
        createdBy: req.user.username,
        createdBy: req.user.username,
      },
      {
        where: { id: i.id, Invoice_ID: inv.id },
      },
      { transaction }
    );
  }
};

exports.deleteJournalEntry = async (req, res, next) => {
  try {
    const { id } = req.params;

    const invLine = await JournalEntry.findByPk(id);
    if (!invLine) {
      return res.status(404).send(errorResponse('Invoice Line not found'));
    }

    JournalEntry.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Invoice Line was deleted',
    });
  } catch (error) {
    next(error);
  }
};
