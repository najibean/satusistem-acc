const { QueryTypes } = require('sequelize');
const models = require('./../models');
const { MasterTax, sequelize } = models;
const { INV_TYPE } = require('./../helpers/constants');

exports.sumFromInvLine = (invLine, key) => {
  if (invLine.length > 0) {
    return invLine.reduce((a, b) => {
      return a + b[key];
    }, 0);
  } else {
    return 0;
  }
};

exports.invoiceLineHandler = async (invLine) => {
  let arr = [];
  for (const i of invLine) {
    const tax = await MasterTax.findByPk(i.MasterTax_ID);
    const PphTax = tax.PphValue * i.Price;
    const PpnTax = tax.PpnValue * i.Price;
    const TotalAmount = i.Quantity * i.Price;
    const GrandTotal = grandTotalHandler(TotalAmount, PpnTax, PphTax, tax.TaxLabel);

    arr.push({
      ChartAccount_ID: i.ChartAccount_ID,
      Name: i.Name,
      Quantity: i.Quantity,
      Price: i.Price,
      PphTax,
      PpnTax,
      MasterTax_ID: i.MasterTax_ID,
      TotalAmount,
      GrandTotal,
      Business_ID: i.Business_ID,
    });
  }

  return arr;
};

const grandTotalHandler = (totalAmount, ppn, pph, taxInclusive) => {
  if (taxInclusive.slice(-9) === 'inclusive') {
    return totalAmount - pph;
  }
  return totalAmount + ppn - pph;
};
