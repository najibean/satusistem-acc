const models = require('../models');
const { Payment, sequelize } = models;
const { errorResponse } = require('../helpers/errorResponse');
const {
  paymentLineHandler,
  sumFromPaymentLine,
  invoiceValidatorHandler,
  paymentOnInvoice,
} = require('./../repositories/paymentRepository');
const { formatDateLocal } = require('./../helpers/dateConverter');
const { createPaymentLine } = require('./paymentLine');
const { createJournalEntryAfterPayment } = require('./journalEntry');
const generateNumber = require('./../helpers/generateNumber');
const { PAYMENT_STATUS } = require('./../helpers/constants');

exports.createPayment = async (req, res, next) => {
  try {
    const body = {
      Partner_ID: req.body.Partner_ID,
      BankAccount_ID: req.body.BankAccount_ID,
      PaymentType: req.body.PaymentType.toUpperCase(),
      PaymentMethod: req.body.PaymentMethod.toUpperCase(),
      DatePayment: formatDateLocal(req.body.DatePayment),
      Description: req.body.Description,
      isActive: req.body.isActive,
      Business_ID: req.body.Business_ID,
      payment_invoice: req.body.payment_invoice,
      createdBy: req.user.username,
    };

    const paymentLines = await paymentLineHandler(body.payment_invoice);

    let paymentCreated;
    await sequelize.transaction(async (t) => {
      Promise.all(
        paymentLines.map((el) => {
          paymentOnInvoice(el.invoice, el.Amount, t);
        })
      );

      const GrandTotal = await sumFromPaymentLine(paymentLines, 'Amount');

      paymentCreated = await Payment.create(
        {
          PaymentNo: await generateNumber(Payment, 'PYM', 'PaymentNo'),
          Status: PAYMENT_STATUS.DRAFT, // jika di approve oleh atasan, baru kemudian jadi SETTLEMENT,
          GrandTotal,
          ...body,
        },
        { transaction: t }
      );

      if (body.payment_invoice.length > 0) {
        await createPaymentLine(req, paymentCreated, paymentLines, t);
      }

      await createJournalEntryAfterPayment(req, paymentCreated, paymentLines, t);
    });

    return res.status(200).send({
      message: 'Payment created',
      data: paymentCreated,
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.getAllPayments = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const pay = await Payment.findAll({
      attributes: {
        exclude: ['updated_at', 'deleted_at'],
      },
      order: [['updated_at', 'DESC']],
    });

    const data = pay.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: pay.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};

exports.getPaymentById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const paymentFound = await Payment.findOne({
      where: { id },
      attributes: {
        exclude: ['deleted_at'],
      },
    });

    if (!paymentFound) {
      return res.status(404).send(errorResponse('Payment not found'));
    }

    return res.status(200).send({
      data: paymentFound,
    });
  } catch (error) {
    next(error);
  }
};

exports.updatePayment = async (req, res, next) => {
  const t = await sequelize.transaction();
  try {
    const { id } = req.params;
    let {
      Partner_ID,
      BankAccount_ID,
      PaymentType,
      PaymentMethod,
      Status,
      DatePayment,
      Description,
      GrandTotal,
      isActive,
      Business_ID,
    } = req.body;

    const paymentFound = await Payment.findByPk(id);
    if (!paymentFound) {
      return res.status(404).send(errorResponse('Payment not found'));
    }

    await Payment.update(
      {
        Partner_ID,
        BankAccount_ID,
        PaymentType: PaymentType.toUpperCase(),
        PaymentMethod: PaymentMethod.toUpperCase(),
        Status: Status.toUpperCase(),
        DatePayment: formatDateLocal(DatePayment),
        Description,
        GrandTotal,
        isActive,
        Business_ID,
        createdBy: req.user.username,
      },
      {
        where: { id },
      },
      { transaction: t }
    );
    await t.commit();

    return res.status(200).send({
      message: 'Payment updated',
      data: await Payment.findOne({
        where: { id },
        attributes: {
          exclude: ['updated_at', 'deleted_at'],
        },
      }),
    });
  } catch (error) {
    next(error);
  }
};

exports.deletePayment = async (req, res, next) => {
  try {
    const { id } = req.params;

    const paymentFound = await Payment.findByPk(id);
    if (!paymentFound) {
      return res.status(404).send(errorResponse('Payment not found'));
    }

    Payment.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Payment was deleted',
    });
  } catch (error) {
    next(error);
  }
};

exports.getByBusinessId = async (req, res, next) => {
  try {
    let page = 1,
      paginate = 10;

    const pay = await Payment.findAll({
      where: {
        Business_ID: req.params.businessid,
      },
    });

    if (pay.length == 0) {
      return res.status(400).send({
        message: `Tax not found on business id ${req.params.businessid}`,
      });
    }

    const data = pay.slice((page - 1) * paginate, page * paginate);

    return res.status(200).send({
      total: pay.length,
      data,
    });
  } catch (error) {
    next(error);
  }
};
