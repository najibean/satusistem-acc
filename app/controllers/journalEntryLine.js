const models = require('../models');
const { JournalEntryLine, ChartAccount, sequelize } = models;
const { Op } = require('sequelize');
const { errorResponse } = require('../helpers/errorResponse');
const { INV_TYPE } = require('./../helpers/constants');
const {
  generateJournalEntryLine,
  sumFromJournalEntryLine,
} = require('../repositories/journalEntryLineRepository');

exports.createJournalEntryLine = async (req, inv, journalEntry, inv_lines, transaction) => {
  let arr = [];

  // define journal entry line per-line with coa
  for (const i of inv_lines) {
    const jelGenerated = await generateJournalEntryLine(i);
    for (const j of jelGenerated) {
      arr.push({
        JournalEntry_ID: journalEntry.id,
        Partner_ID: inv.Partner_ID,
        ChartAccount_ID: j.ChartAccount_ID,
        description: j.description,
        debit: j.debit,
        credit: j.credit,
        isActive: 1,
        Business_ID: i.Business_ID,
        createdBy: req.user.username,
      });
    }
  }

  // define total hutang/piutang usaha with coa (pembelian di kredit, penjualan di debit)
  if (inv.InvoiceType === INV_TYPE.PURCHASE) {
    const coa = await ChartAccount.findOne({
      where: { AccountNumber: '2104', Business_ID: inv.Business_ID },
    }); // hutang usaha

    arr.push({
      JournalEntry_ID: journalEntry.id,
      Partner_ID: inv.Partner_ID,
      ChartAccount_ID: coa.id,
      description: `${coa.AccountName} dari Invoice No. ${inv.InvoiceNo}`,
      debit: 0,
      credit: sumFromJournalEntryLine(arr, 'debit') - sumFromJournalEntryLine(arr, 'credit'),
      isActive: 1,
      Business_ID: inv.Business_ID,
      createdBy: req.user.username,
    });
  } else if (inv.InvoiceType === INV_TYPE.SALES) {
    const coa = await ChartAccount.findOne({
      where: { AccountNumber: '2103', Business_ID: inv.Business_ID },
    }); // piutang usaha

    arr.push({
      JournalEntry_ID: journalEntry.id,
      Partner_ID: inv.Partner_ID,
      ChartAccount_ID: coa.id,
      description: `${coa.AccountName} dari Invoice No. ${inv.InvoiceNo}`,
      debit: sumFromJournalEntryLine(arr, 'credit') - sumFromJournalEntryLine(arr, 'debit'),
      credit: 0,
      isActive: 1,
      Business_ID: inv.Business_ID,
      createdBy: req.user.username,
    });
  }

  // create every journal entry per line per Business_ID
  for (const k of arr) {
    await JournalEntryLine.create(
      {
        JournalEntry_ID: journalEntry.id,
        Partner_ID: inv.Partner_ID,
        ChartAccount_ID: k.ChartAccount_ID,
        description: k.description,
        debit: k.debit,
        credit: k.credit,
        isActive: k.isActive,
        Business_ID: k.Business_ID,
        createdBy: k.createdBy,
      },
      {
        transaction,
      }
    );
  }
};

exports.createJournalEntryLineAfterPayment = async (
  req,
  payment,
  journalEntry,
  paymentLines,
  t
) => {
  /**
   * 2104 ---> kode chart account untuk Hutang Usaha
   * dan, diambil berdasarkan Business_ID
   */
  const coaFound = await ChartAccount.findOne({
    where: {
      AccountNumber: '2104',
      Business_ID: payment.Business_ID,
    },
  });

  let arr = [];

  // define journal entry line per-line payments
  paymentLines.map((el) => {
    arr.push({
      JournalEntry_ID: journalEntry.id,
      Partner_ID: payment.Partner_ID,
      ChartAccount_ID: coaFound.id,
      description: `${coaFound.AccountName} dikredit dari Invoice No. ${el.invoice.InvoiceNo}`,
      debit: 0,
      credit: el.Amount,
      isActive: 1,
      Business_ID: payment.Business_ID,
      createdBy: req.user.username,
    });
  });

  // define total hutang usaha with coa (pembelian di kredit, penjualan di debit)
  arr.push({
    JournalEntry_ID: journalEntry.id,
    Partner_ID: payment.Partner_ID,
    ChartAccount_ID: coaFound.id,
    description: `${coaFound.AccountName} didebit dari payment dengan Invoice No: ${paymentLines
      .map((el) => el.invoice.InvoiceNo)
      .join(', ')}`,
    debit: sumFromJournalEntryLine(paymentLines, 'Amount'),
    credit: 0,
    isActive: 1,
    Business_ID: payment.Business_ID,
    createdBy: req.user.username,
  });

  // create every journal entry per line per Business_ID
  Promise.all(
    arr.map(async (el) => {
      await JournalEntryLine.create(
        {
          JournalEntry_ID: journalEntry.id,
          Partner_ID: payment.Partner_ID,
          ChartAccount_ID: el.ChartAccount_ID,
          description: el.description,
          debit: el.debit,
          credit: el.credit,
          isActive: el.isActive,
          Business_ID: el.Business_ID,
          createdBy: el.createdBy,
        },
        {
          transaction: t,
        }
      );
    })
  );
};

exports.updateJournalEntryLine = async (req, inv, inv_lines, transaction) => {
  for (const i of inv_lines) {
    await JournalEntryLine.update(
      {
        ChartAccount_ID: i.ChartAccount_ID,
        Name: i.Name,
        Quantity: i.Quantity,
        Price: i.Price,
        PphTax: i.PphTax,
        PpnTax: i.PpnTax,
        MasterTax_ID: i.MasterTax_ID,
        TotalAmount: i.TotalAmount,
        Business_ID: i.Business_ID,
        createdBy: req.user.username,
        createdBy: req.user.username,
      },
      {
        where: { id: i.id, Invoice_ID: inv.id },
      },
      { transaction }
    );
  }
};

exports.deleteJournalEntryLine = async (req, res, next) => {
  try {
    const { id } = req.params;

    const invLine = await JournalEntryLine.findByPk(id);
    if (!invLine) {
      return res.status(404).send(errorResponse('Invoice Line not found'));
    }

    JournalEntryLine.destroy({ where: { id } });

    return res.status(200).send({
      message: 'Invoice Line was deleted',
    });
  } catch (error) {
    next(error);
  }
};
