'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class Partner extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Invoice, {
        foreignKey: 'Partner_ID',
        as: 'invoice',
      });

      this.hasMany(models.JournalEntryLine, {
        foreignKey: 'Partner_ID',
        as: 'journal_entry_line',
      });

      this.hasMany(models.Order, {
        foreignKey: 'Partner_ID',
        as: 'order',
      });

      this.hasMany(models.Payment, {
        foreignKey: 'Partner_ID',
        as: 'payment',
      });
    }
  }
  Partner.init(
    {
      PartnerName: DataTypes.STRING,
      PartnerNo: DataTypes.STRING,
      TaxNo: DataTypes.STRING,
      Email: DataTypes.STRING,
      Address: DataTypes.STRING,
      City: DataTypes.STRING,
      PhoneNo: DataTypes.STRING,
      Province: DataTypes.STRING,
      Zipcode: DataTypes.STRING,
      PartnerType: DataTypes.STRING,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Partner',
      tableName: 'MPartner',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  Partner.beforeCreate((el) => (el.id = uuidv4()));
  return Partner;
};
