const router = require('express').Router();
const { isAuthenticate } = require('../middlewares/authMiddleware');
const { internalTransfer } = require('../controllers/bankTransaction');
const { validate } = require('../validators');
const { paymentValidator } = require('../validators/paymentValidator');

/**
 * @swagger
 * components:
 *  schemas:
 *    BankTransaction:
 *      type: object
 *      required:
 *        - toBankAccId
 *        - amount
 *        - reference
 *      properties:
 *        toBankAccId:
 *          type: string
 *          description: id of bank account
 *        amount:
 *          type: double
 *          description: amount
 *        reference:
 *          type: string
 *          description: notes
 *        Business_ID:
 *          type: string
 *          description: bussiness id
 *      example:
 *        toBankAccId: 4a6d5bae-2838-4e2e-b9c1-6bb329ce5095
 *        amount: 3000
 *        reference: test internal transfer
 *        Business_ID: Business_ID
 *  parameters:
 *    BankTransaction_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Bank transaction ID
 *    bankAccount_id:
 *      in: path
 *      name: bankAccount_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Bank account ID
 */

router.use(isAuthenticate);

/*
 * @swagger
 * /api/payment/all:
 *  get:
 *    summary: Returns the list of all payments
 *    tags: [BankTransaction]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of payments
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/Payment'
 */
// router.get('/all');

/**
 * @swagger
 * /api/bank-transaction/internal-transfer/{bankAccount_id}:
 *  post:
 *    summary: Create internal transfer
 *    tags: [BankTransaction]
 *    parameters:
 *      - $ref: '#/components/parameters/bankAccount_id'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/BankTransaction'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Internal transfer/pindah buku done
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/BankTransaction'
 *      500:
 *        description: Some server error
 */
router.post('/internal-transfer/:id', internalTransfer);

/*
 * @swagger
 * /api/payment/{id}:
 *  get:
 *    summary: Get payment by id
 *    tags: [Payment]
 *    parameters:
 *      - $ref: '#/components/parameters/Payment_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of payment
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Payment'
 *      404:
 *        description: Partner not found
 */
// router.get('/internal-transfer', internalTransfer);

/*
 * @swagger
 * /api/payment/{id}:
 *  patch:
 *    summary: Update Payment by the id
 *    tags: [Payment]
 *    parameters:
 *      - $ref: '#/components/parameters/Payment_ID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Payment'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Payment was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Payment'
 *      404:
 *        description: Payment not found
 *      500:
 *        description: Some server error
 */
// router.patch('/:id', validate(paymentValidator), updatePayment);

/*
 * @swagger
 * /api/payment/{id}:
 *  delete:
 *    summary: Remove Payment by id
 *    tags: [Payment]
 *    parameters:
 *      - $ref: '#/components/parameters/Payment_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Payment was deleted
 *      404:
 *        description: Payment not found
 */
// router.delete('/:id', deletePayment);

/*
 * @swagger
 * /api/payment/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of payments based on business id
 *    tags: [Payment]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of payments based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/business_id'
 */
// router.get('/all/by-business-id/:businessid', getByBusinessId);

module.exports = router;
