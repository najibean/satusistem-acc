'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Invoice, {
        foreignKey: 'Order_ID',
        as: 'invoice',
      });

      this.belongsTo(models.Partner, {
        foreignKey: 'Partner_ID',
        as: 'partner',
      });

      this.hasMany(models.OrderLine, {
        foreignKey: 'Order_ID',
        as: 'order_line',
      });
    }
  }
  Order.init(
    {
      OrderNo: DataTypes.STRING,
      OrderDate: DataTypes.DATE,
      DeliveryDate: DataTypes.DATE,
      OrderRef: DataTypes.STRING,
      Partner_ID: DataTypes.UUID,
      Subtotal: DataTypes.DOUBLE,
      Discount: DataTypes.DOUBLE,
      PphTax: DataTypes.DOUBLE,
      PpnTax: DataTypes.DOUBLE,
      GrandTotal: DataTypes.DOUBLE,
      OrderType: DataTypes.STRING,
      OrderStatus: DataTypes.STRING,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Order',
      tableName: 'MOrder',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  Order.beforeCreate((el) => (el.id = uuidv4()));
  return Order;
};
