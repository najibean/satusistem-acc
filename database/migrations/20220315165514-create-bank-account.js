'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MBankAccount', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      ListBank_ID: {
        type: Sequelize.UUID
      },
      NamaAccount: {
        type: Sequelize.STRING
      },
      NoAccount: {
        type: Sequelize.INTEGER
      },
      BranchName: {
        type: Sequelize.STRING
      },
      BranchCity: {
        type: Sequelize.STRING
      },
      ChartAccount_ID: {
        type: Sequelize.UUID
      },
      MasterCurrency_ID: {
        type: Sequelize.UUID
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MBankAccount', {
      fields: ['ListBank_ID'],
      type: 'foreign key',
      name: 'fk_bankaccount_listbank_id',
      references: {
        table: 'MListBank',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MBankAccount', {
      fields: ['ChartAccount_ID'],
      type: 'foreign key',
      name: 'fk_bankaccount_chartaccount_id',
      references: {
        table: 'MChartAccount',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MBankAccount', {
      fields: ['MasterCurrency_ID'],
      type: 'foreign key',
      name: 'fk_bankaccount_mastercurrency_id',
      references: {
        table: 'MMasterCurrency',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MBankAccount');
  }
};