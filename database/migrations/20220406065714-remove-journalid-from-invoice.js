'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.removeConstraint('MInvoice', 'fk_invoice_journalentry_id');

    await queryInterface.removeColumn('MInvoice', 'JournalEntry_ID');
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.addColumn('MInvoice', 'JournalEntry_ID', {
      type: Sequelize.UUID,
      after: 'isActive',
    });

    await queryInterface.addConstraint('MInvoice', {
      fields: ['JournalEntry_ID'],
      type: 'foreign key',
      name: 'fk_invoice_journalentry_id',
      references: {
        table: 'MJournalEntry',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
};
