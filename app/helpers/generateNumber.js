const seqNumb = (journalNumb) => {
  return String(Number(journalNumb.slice(3)) + 1).padStart(5, 0);
};

module.exports = async (model, prefix, nameColumn) => {
  const found = await model.findAll({
    order: [['created_at', 'ASC']],
  });

  if (found.length > 0) {
    const lastPaymentNo = found[found.length - 1][nameColumn];
    return prefix + seqNumb(lastPaymentNo);
  } else {
    return `${prefix}00001`;
  }
};
