'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class ListBank extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.BankAccount, {
        foreignKey: 'ListBank_ID',
        as: 'bank_account'
      })
    }
  }
  ListBank.init(
    {
      NamaBank: DataTypes.STRING,
      SwiftCode: DataTypes.STRING,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'ListBank',
      tableName: 'MListBank',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  ListBank.beforeCreate((el) => (el.id = uuidv4()));
  return ListBank;
};
