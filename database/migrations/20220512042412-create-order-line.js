'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MOrderLine', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      Order_ID: {
        type: Sequelize.UUID,
      },
      Name: {
        type: Sequelize.STRING,
      },
      Quantity: {
        type: Sequelize.DOUBLE,
      },
      Price: {
        type: Sequelize.DOUBLE,
      },
      PphTax: {
        type: Sequelize.DOUBLE,
      },
      PpnTax: {
        type: Sequelize.DOUBLE,
      },
      MasterTax_ID: {
        type: Sequelize.UUID,
      },
      TotalAmount: {
        type: Sequelize.DOUBLE,
      },
      Business_ID: {
        type: Sequelize.STRING,
      },
      createdBy: {
        type: Sequelize.STRING,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MOrderLine', {
      fields: ['Order_ID'],
      type: 'foreign key',
      name: 'fk_orderline_order_id',
      references: {
        table: 'MOrder',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MOrderLine', {
      fields: ['MasterTax_ID'],
      type: 'foreign key',
      name: 'fk_orderline_mastertax_id',
      references: {
        table: 'MMasterTax',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MOrderLine');
  },
};
