'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MInvoiceLine', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
      },
      Invoice_ID: {
        type: Sequelize.UUID
      },
      ChartAccount_ID: {
        type: Sequelize.UUID
      },
      Name: {
        type: Sequelize.STRING
      },
      Quantity: {
        type: Sequelize.DOUBLE
      },
      Price: {
        type: Sequelize.DOUBLE
      },
      PphTax: {
        type: Sequelize.DOUBLE
      },
      PpnTax: {
        type: Sequelize.DOUBLE
      },
      MasterTax_ID: {
        type: Sequelize.UUID
      },
      TotalAmount: {
        type: Sequelize.DOUBLE
      },
      Business_ID: {
        type: Sequelize.STRING
      },
      createdBy: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addConstraint('MInvoiceLine', {
      fields: ['Invoice_ID'],
      type: 'foreign key',
      name: 'fk_invoiceline_invoice_id',
      references: {
        table: 'MInvoice',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MInvoiceLine', {
      fields: ['ChartAccount_ID'],
      type: 'foreign key',
      name: 'fk_invoiceline_chartaccount_id',
      references: {
        table: 'MChartAccount',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });

    await queryInterface.addConstraint('MInvoiceLine', {
      fields: ['MasterTax_ID'],
      type: 'foreign key',
      name: 'fk_invoiceline_mastertax_id',
      references: {
        table: 'MMasterTax',
        field: 'id',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MInvoiceLine');
  }
};