const { body } = require('express-validator');
const { INV_TYPE, INV_STATUS } = require('./../helpers/constants');
const models = require('./../models');
const { MasterTax, ChartAccount, Order, MasterCurrency, InvoiceLine, Partner, sequelize } = models;

const invoiceValidator = [
  body('InvoiceDate')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isISO8601()
    .toDate()
    .withMessage('Please use the correct date format (YYYY-MM-DD)'),
  body('DueDate')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isISO8601()
    .toDate()
    .withMessage('Please use the correct date format (YYYY-MM-DD)'),
  body('InvoiceType')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom((value, { req }) => {
      let arr = [];
      for (const key in INV_TYPE) {
        arr.push(INV_TYPE[key]);
      }

      if (!Object.values(INV_TYPE).includes(value.toUpperCase())) {
        throw new Error('Please input: ' + arr.join(' or '));
      }

      return true;
    }),
  body('Order_ID')
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const order = await Order.findByPk(value);

      if (!order) {
        throw new Error('Please input the correct ID of Order');
      }

      return true;
    }),
  body('MasterCurrency_ID')
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const currency = await MasterCurrency.findByPk(value);

      if (!currency) {
        throw new Error('Please input the correct ID of Master Currency');
      }

      return true;
    }),
  body('Partner_ID')
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const partner = await Partner.findByPk(value);

      if (!partner) {
        throw new Error('Please input the correct ID of Partner');
      }

      return true;
    }),
  body('Discount').isNumeric().withMessage('Must be contain number only'),
  body('isActive').isNumeric().withMessage('Must be contain number only'),
  body('Business_ID').isString().withMessage('Must be string'),
  body('invoice_line.*.MasterTax_ID')
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const tax = await MasterTax.findByPk(value);

      if (!tax) {
        throw new Error('Please input the correct ID of MasterTax');
      }

      return true;
    }),
  body('invoice_line.*.ChartAccount_ID')
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const tax = await ChartAccount.findByPk(value);

      if (!tax) {
        throw new Error('Please input the correct ID of ChartAccount');
      }

      return true;
    }),
  body('invoice_line.*.Name').isString().withMessage('Must be string'),
  body('invoice_line.*.Quantity').isNumeric().withMessage('Must be contain number only'),
  body('invoice_line.*.Price').isNumeric().withMessage('Must be contain number only'),
  body('invoice_line.*.Business_ID').isString().withMessage('Must be string'),
];

const updateInvoiceValidator = [
  ...invoiceValidator,
  body('invoice_line.*.id')
    .isString()
    .withMessage('Must be string')
    .bail()
    .custom(async (value, { req }) => {
      const invLine = await InvoiceLine.findByPk(value);

      if (!invLine) {
        throw new Error('Please input the correct ID of Invoice Line');
      }

      return true;
    }),
];

module.exports = { invoiceValidator, updateInvoiceValidator };
