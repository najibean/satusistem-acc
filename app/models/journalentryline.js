'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class JournalEntryLine extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.ChartAccount, {
        foreignKey: 'ChartAccount_ID',
        as: 'chart_account'
      })

      this.belongsTo(models.JournalEntry, {
        foreignKey: 'JournalEntry_ID',
        as: 'journal_entry'
      })

      this.belongsTo(models.Partner, {
        foreignKey: 'Partner_ID',
        as: 'partner'
      })
    }
  }
  JournalEntryLine.init(
    {
      JournalEntry_ID: DataTypes.UUID,
      Partner_ID: DataTypes.UUID,
      ChartAccount_ID: DataTypes.UUID,
      description: DataTypes.STRING,
      debit: DataTypes.DOUBLE,
      credit: DataTypes.DOUBLE,
      isActive: DataTypes.INTEGER,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'JournalEntryLine',
      tableName: 'MJournalEntryLine',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  JournalEntryLine.beforeCreate((el) => (el.id = uuidv4()));
  return JournalEntryLine;
};
