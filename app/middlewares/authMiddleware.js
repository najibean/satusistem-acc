const jwt = require('jsonwebtoken');
const { errorResponse } = require('./../helpers/errorResponse');

exports.isAuthenticate = async (req, res, next) => {
  if (!req.headers.authorization) {
    return res.status(401).send('Unauthorized');
  }

  const splitToken = req.headers.authorization.split(' ');
  if (splitToken.length !== 2 || splitToken[0] !== 'Bearer') {
    return res.status(400).send('Wrong authorization format');
  }

  jwt.verify(
    splitToken[1],
    process.env.SECRET_KEY_JWT,
    { algorithms: ['HS256'] },
    async (err, payload) => {
      if (err && err.name === 'TokenExpiredError') {
        return res.status(401).send('Expired Token');
      } else if (err) {
        return res.status(401).send('Invalid Token');
      } else {
        try {
          req.user = payload;
          next();
        } catch (error) {
          next(error);
        }
      }
    }
  );
};
