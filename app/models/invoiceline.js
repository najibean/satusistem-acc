'use strict';
const { Model } = require('sequelize');
const { v4: uuidv4 } = require('uuid');
module.exports = (sequelize, DataTypes) => {
  class InvoiceLine extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.ChartAccount, {
        foreignKey: 'ChartAccount_ID',
        as: 'chart_account'
      })

      this.belongsTo(models.Invoice, {
        foreignKey: 'Invoice_ID',
        as: 'invoice'
      })

      this.belongsTo(models.MasterTax, {
        foreignKey: 'MasterTax_ID',
        as: 'master_tax'
      })
    }
  }
  InvoiceLine.init(
    {
      Invoice_ID: DataTypes.UUID,
      ChartAccount_ID: DataTypes.UUID,
      Name: DataTypes.STRING,
      Quantity: DataTypes.DOUBLE,
      Price: DataTypes.DOUBLE,
      PphTax: DataTypes.DOUBLE,
      PpnTax: DataTypes.DOUBLE,
      MasterTax_ID: DataTypes.UUID,
      TotalAmount: DataTypes.DOUBLE,
      Business_ID: DataTypes.STRING,
      createdBy: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'InvoiceLine',
      tableName: 'MInvoiceLine',
      paranoid: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
    }
  );

  InvoiceLine.beforeCreate((el) => (el.id = uuidv4()));
  return InvoiceLine;
};
