const { body } = require('express-validator');
const models = require('../models');
const { Partner, sequelize } = models;
const businessIdValidator = require('./businessIdValidator');

const partnerValidator = [
  body('PartnerName')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('TaxNo')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('Address')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('City')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('PhoneNo')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('Province')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('Zipcode')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('PartnerType')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isString()
    .withMessage('Must be string'),
  body('isActive')
    .optional({ checkFalsy: true })
    .bail()
    .custom((value, { req }) => {
      if (typeof value !== 'number') {
        throw new Error('Must be number type');
      }

      if (![0, 1].includes(value)) {
        throw new Error('Value must be 0 or 1');
      }

      return true;
    }),
  body('Email')
    .notEmpty()
    .withMessage('Can not be empty')
    .bail()
    .isEmail()
    .withMessage('Must be in email format'),
];

const createPartnerValidator = [...partnerValidator, ...businessIdValidator];

module.exports = { partnerValidator, createPartnerValidator };
