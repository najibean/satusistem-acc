const router = require('express').Router();
const { isAuthenticate } = require('./../middlewares/authMiddleware');
const {
  createChartAccount,
  getAllChartAccounts,
  getChartAccountById,
  updateChartAccount,
  deleteChartAccount,
  getByBusinessId,
} = require('./../controllers/chartAccount');

/**
 * @swagger
 * components:
 *  schemas:
 *    ChartAccount:
 *      type: object
 *      required:
 *        - AccountName
 *        - AccountNumber
 *        - HeaderID
 *        - AccountType
 *        - Business_ID
 *      properties:
 *        AccountName:
 *          type: string
 *          description: Name of account
 *        AccountNumber:
 *          type: integer
 *          description: Number of account
 *        HeaderID:
 *          type: string
 *          description: Header's ID of account
 *        AccountType:
 *          type: string
 *          description: Type of account
 *        Business_ID:
 *          type: string
 *          description: Bussiness ID of account
 *      example:
 *        AccountName: chart_account_name
 *        AccountNumber: 325632562
 *        HeaderID: header_id_123
 *        AccountType: account_type
 *        Business_ID: Business_ID_001
 *  parameters:
 *    ChartAccount_ID:
 *      in: path
 *      name: id
 *      schema:
 *        type: string
 *      required: true
 *      description: Chart Account ID
 *    business_id:
 *      in: path
 *      name: business_id
 *      schema:
 *        type: string
 *      required: true
 *      description: Business ID
 */

router.use(isAuthenticate);

/**
 * @swagger
 * /api/coa/all:
 *  get:
 *    summary: Returns the list of all chart accounts
 *    tags: [ChartAccount]
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of the chart accounts
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/ChartAccount'
 */
router.get('/all', getAllChartAccounts);

/**
 * @swagger
 * /api/coa/create:
 *  post:
 *    summary: Create new chart account
 *    tags: [ChartAccount]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/ChartAccount'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Chart account was successfully created
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ChartAccount'
 *      500:
 *        description: Some server error
 */
router.post('/create', createChartAccount);

/**
 * @swagger
 * /api/coa/{id}:
 *  get:
 *    summary: Get chart account by id
 *    tags: [ChartAccount]
 *    parameters:
 *      - $ref: '#/components/parameters/ChartAccount_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Detail of chart account
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ChartAccount'
 *      404:
 *        description: Partner not found
 */
router.get('/:id', getChartAccountById);

/**
 * @swagger
 * /api/coa/{id}:
 *  patch:
 *    summary: Update chart account by the id
 *    tags: [ChartAccount]
 *    parameters:
 *      - $ref: '#/components/parameters/ChartAccount_ID'
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/ChartAccount'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Chart Account was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ChartAccount'
 *      404:
 *        description: Chart Account not found
 *      500:
 *        description: Some server error
 */
router.patch('/:id', updateChartAccount);

/**
 * @swagger
 * /api/coa/{id}:
 *  delete:
 *    summary: Remove Chart Account by id
 *    tags: [ChartAccount]
 *    parameters:
 *      - $ref: '#/components/parameters/ChartAccount_ID'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: Chart Account was deleted
 *      404:
 *        description: Chart Account not found
 */
router.delete('/:id', deleteChartAccount);

/**
 * @swagger
 * /api/coa/all/by-business-id/{business_id}:
 *  get:
 *    summary: Returns the list of chart_accounts based on business id
 *    tags: [ChartAccount]
 *    parameters:
 *      - $ref: '#/components/parameters/business_id'
 *    responses:
 *      401:
 *        description: Unauthorized
 *      200:
 *        description: The list of chart_accounts based on business id
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/business_id'
 */
router.get('/all/by-business-id/:businessid', getByBusinessId);

module.exports = router;
