const pagination = require('./pagination');
const { BadRequestException } = require('./customException');

module.exports = async (model, modelName, req, res, next) => {
  try {
    const { page, paginate } = req.query;

    const datas = await model.findAll({
      where: {
        Business_ID: req.params.businessid,
      },
      ...pagination(page, paginate),
    });

    if (datas.length == 0) {
      throw new BadRequestException(`${modelName} not found on this business id`);
    }

    return res.status(200).send({
      total: datas.length,
      data: datas,
    });
  } catch (error) {
    next(error);
  }
};
